from data.database import read_query_single

def location_exists(city):
    location_list = read_query_single("select id from Locations where city = ?", (city,))
    if location_list is None:
        return False
    return True

def get_location_by_id(id:int) -> str:
    location = read_query_single('SELECT city FROM locations WHERE id=?',(id,))
    return location[0]

def get_location_id_by_city(city) -> str:
    location = read_query_single('SELECT id FROM locations WHERE city=?',(city,))
    if location:
        return location[0]
    return None

