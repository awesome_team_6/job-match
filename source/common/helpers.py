from common import config
from dotenv import load_dotenv

from common.config import MJ_API_KEY, MJ_SECRET

load_dotenv()
import cloudinary
import cloudinary.uploader
import cloudinary.api
import json
import os
from mailjet_rest import Client
from pydantic import BaseModel, EmailStr

config = cloudinary.config(secure=True)


def upload_photo(file_url, name):
    cloudinary.uploader.upload(file_url, public_id=name, unique_filename=True)

    # Build the URL for the image and save it in the variable 'srcURL'
    srcURL = cloudinary.CloudinaryImage(name).build_url()

    return srcURL
    # Log the image URL to the console. 
    # Copy this URL in a browser tab to generate the image on the fly.
    print("****2. Upload an image****\nDelivery URL: ", srcURL, "\n")


# file = '../Final_project/test.png'

# cloudinary.uploader.upload("test.png", 
#   folder = "../Final_project/", 
#   public_id = "test",
#   resource_type = "image")

# def upload_photo(photo_location: str):
#     result = cloudinary.uploader.upload(file)
#     print(result)
#     return result

def send_mail(request_receiver, request_sender, id):
    api_key = MJ_API_KEY
    api_secret = MJ_SECRET
    mailjet = Client(auth=(api_key, api_secret), version='v3.1')
    data = {
        'Messages': [
            {
                "From": {
                    "Email": "ppetkof1983@gmail.com",
                    "Name": "Pavel"
                },
                "To": [
                    {
                        "Email": "ppetkof1983@gmail.com",
                        "Name": "Pavel"
                    }
                ],
                "Subject": "Notification for a match request.",
                "TextPart": "Notification for a match request",
                "HTMLPart": f"<h3>Dear {request_receiver}, you have a match request from {request_sender} on your ad with id: {id}",
                "CustomID": "AppGettingStartedTest"
            }
        ]
    }
    result = mailjet.send.create(data=data)

    return result.json()


