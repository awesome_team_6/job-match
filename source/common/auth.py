import jwt
from fastapi import HTTPException
from common.config import JWT_SECRET
from data.database import read_query
from data.models import Professional, Employer

def _hash_password(password: str):
    from hashlib import sha256
    return sha256(password.encode('utf-8')).hexdigest()

def get_user_or_raise_401(token: str, type: str) -> Professional | Employer | None:
    if not is_authenticated(token, type):
        raise HTTPException(status_code=401)
    return from_token(token, type)

def create_token(user: Employer | Professional) -> str:
    encoded_jwt = jwt.encode({"user": user.username}, JWT_SECRET, algorithm="HS256")
    return encoded_jwt

def from_token(token: str, type) -> Professional | Employer | None:
    username = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])["user"]
    return find_by_username(username, type)

def is_authenticated(token: str, type) -> bool:
    decoded_jwt = jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
    return any(read_query(
        f'SELECT 1 FROM {type} WHERE username = ?',
        (decoded_jwt["user"],)))

def find_by_username(username: str, type: str) -> Professional | Employer | None:
    data = read_query( 
            f'SELECT * FROM {type} WHERE username = ?',
            (username,))
    if type == "professionals":
        return next((Professional.from_query_result(*row) for row in data), None)
    if type == "employers":          
        return next((Employer.from_query_result(*row) for row in data), None)

