from common.responses import BadRequest
from data.database import insert_query, read_query, update_query, read_query_single
from data.models import Job_ad, Employer, QualificationLevel, Job_adResponse, New_Job_ad, QualificationLevels, JobResponseModel

def create(job_ad: JobResponseModel, employer: Employer):
    job_ad.employers_id = employer.id
    generated_id = insert_query(
        'INSERT INTO job_ads(minimum_salary, maximum_salary, description, is_full_remote,is_partial_remote, location_id, status_id, employers_id) VALUES(?,?,?,?,?,?,?,?)',
        (job_ad.minimum_salary, job_ad.maximum_salary, job_ad.description, job_ad.is_full_remote,job_ad.is_partial_remote, job_ad.location_id, job_ad.status_id, job_ad.employers_id))

    job_ad.id = generated_id

    add_qualification_levels(job_ad.id,job_ad.qualification_levels)

    return job_ad

def add_new_qualifications(qualification: str):
    generated_id = insert_query('INSERT INTO qualifications(qualification) VALUES(?)', (qualification,))

def qualification_exist(qualification):
    data = read_query_single("SELECT id from qualifications where qualification = ?", (qualification,))
    if data is None:
        return False
    return True

def level_exist(level):
    data = read_query_single("SELECT id from levels_of_qualification where level = ?", (level,))
    if data is None:
        return False
    return True

def insert_match_requests(job_ads_id:int, professionals_ids: list[int]):
    relations = ','.join(
        f'({job_ads_id},{professionals_id})' for professionals_id in professionals_ids)
    insert_query(
        f'INSERT INTO job_ads_matches(job_ads_id, professionals_id) VALUES {relations}')
    
    
def insert_qualifications(job_ads_id: int, qualification_levels:list[QualificationLevel]):
    relations = ','.join(
        f'({QualificationLevel.qualifications_id},{QualificationLevel.levels_of_qualification_id}, {job_ads_id})' for  QualificationLevel in qualification_levels)
    insert_query(
        f'INSERT INTO qualifications_levels(qualifications_id,levels_of_qualification_id, job_ads_id) VALUES {relations}')

def get_qualifications(job_ads_id: int) -> list[QualificationLevel]:
    data = read_query(
        '''Select qualifications_id, levels_of_qualification_id FROM qualifications_levels WHERE job_ads_id= ?''',
        (job_ads_id,))

    return [QualificationLevel.from_query_result(*row) for row in data]

def add_qualification_levels(job_ads_id: int, qualification_levels:list[QualificationLevels]):
    
    for element in qualification_levels:
        if qualification_exist(element.qualification) is False:
            add_new_qualifications(element.qualification)
        if level_exist(element.level) is False:
            return 'No such level'
    
    for element in qualification_levels: 
        data = read_query('SELECT qualifications.id,levels_of_qualification.id from qualifications cross join levels_of_qualification where qualifications.qualification = ? and levels_of_qualification.level = ?',(element.qualification,element.level))
        result = [QualificationLevel.from_query_result(*row) for row in data]       
        insert_qualifications(job_ads_id,result)

def create_response_object(job_ad: Job_ad, employer: Employer, professional_qualifications_levels: list[QualificationLevel]):

    return {
        'id': job_ad.id,
        'minimum_salary':job_ad.minimum_salary,
        'maximum_salary':job_ad.maximum_salary,
        'employers_id': employer.id,
        'description': job_ad.description,
        'qualifications_ids': professional_qualifications_levels
    }

def get_by_id(id: int):
    data = read_query('SELECT id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id FROM job_ads where id = ?', (id,))
    flattened = {}
    
    for id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id in data:
        
        if id not in flattened:
            
            flattened[id] = (id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id,[])
            qualifications_ids = get_qualifications(id)
            flattened[id][-1].append(qualifications_ids)
     
   
    return next((Job_adResponse.from_query_result(*obj) for obj in flattened.values()), None)

def get_by_id_model(id: int):
    data = read_query('SELECT id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id FROM job_ads where id = ?', (id,))
    
    return next((New_Job_ad.from_query_result(*obj) for obj in data), None)  

def qualification_exists(id: int):
    data = read_query('SELECT 1 from qualifications where id = ?', (id,))

    return any(data)

def update(old: Job_ad, new: Job_ad):

    merged = Job_ad(
        id = old.id,
        minimum_salary = new.minimum_salary or old.minimum_salary,
        maximum_salary = new.maximum_salary or old.maximum_salary,
        description = new.description or old.description,
        is_full_remote = new.is_full_remote or old.is_full_remote,
        is_partial_remote = new.is_partial_remote or old.is_partial_remote,
        location_id = new.location_id or old.location_id,
        status_id = new.status_id or old.status_id)

    update_query(
        '''UPDATE job_ads SET
           minimum_salary=?, maximum_salary=?, description=?, is_full_remote=?,  is_partial_remote=?, location_id=?, status_id=? 
           WHERE id = ?
        ''',
        (merged.minimum_salary, merged.maximum_salary, merged.description, merged.is_full_remote,  merged.is_partial_remote, merged.location_id, merged.status_id, merged.id))

    return merged

def get_job_ad_status(id:int):
    data = read_query_single('''SELECT status FROM status JOIN job_ads ON status.id = job_ads.status_id where job_ads.id=?''', (id,))[0]
    status = data
    return status

def get_all(location, salary_min, salary_max, salary_threshold, skills_str, skills_threshold, page_number: int | None = None, page_size: int | None = None):
    if page_number is None:
        page_number = 1
    if page_size is None:
        page_size = 10
    page_number = int(page_number)
    page_size = int(page_size)
    start_page = (page_number-1) * page_size
    end_page = start_page + page_size

    base_query = 'SELECT id, minimum_salary, maximum_salary, description,is_full_remote,\
        is_partial_remote,location_id,status_id,employers_id FROM job_ads as ja LEFT JOIN qualifications_levels AS ql ON ja.id = ql.job_ads_id where status_id = 1'
    sql_args = []
    if not location and not salary_min and not salary_max and not skills_str:    
        data = read_query('''SELECT id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id 
            FROM job_ads AS ja LEFT JOIN qualifications_levels AS ql 
            ON ja.id = ql.job_ads_id where status_id = 1
            ''')

    if location:
        location_id= get_location_id(location)
        if location_id is None:
            return BadRequest("Please select valid location")
        base_query += ' AND (location_id=? OR is_full_remote=?)'
        sql_args.append(location_id)
        sql_args.append(1)

    if salary_min or salary_max:
        if salary_threshold:
            salary_min = int(salary_min - salary_min * (salary_threshold/100))
            salary_max = int(salary_max + salary_max * (salary_threshold/100))
        if location: 
            base_query += ' AND ((minimum_salary BETWEEN ? AND ? OR  maximum_salary BETWEEN ? AND ?) or (minimum_salary<= ? and maximum_salary>= ?))'
        else: 
            base_query += ' AND (minimum_salary BETWEEN ? AND ? OR  maximum_salary BETWEEN ? AND ? or (minimum_salary<= ? and maximum_salary>= ?))'
        sql_args.append(salary_min)
        sql_args.append(salary_max)
        sql_args.append(salary_min)
        sql_args.append(salary_max)
        sql_args.append(salary_min)
        sql_args.append(salary_max)

    if skills_str:
        skills = skills_str.split(', ')
        skills_count = len(skills)
        if skills_threshold:
            skills_count -= skills_threshold 

    data = read_query(base_query, tuple(sql_args))
    flattened = {}
    to_remove = []
    
    for id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id in data:
        
        if id not in flattened:
            
            flattened[id] = (id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id,[])
            if skills_str:
                qualifications_ids = get_qualifications_test(id, skills, skills_count)
                if not qualifications_ids: #try with if not qualifications_ids
                    to_remove.append(id)      
            else:
                # combine get_qualifications_test and get_qualifications in one method -> prettier
                qualifications_ids = get_qualifications(id)
            flattened[id][-1].append(qualifications_ids)
        
    for id in to_remove:
        del flattened[id]
    
    return (Job_adResponse.from_query_result(*obj) for obj in list(flattened.values())[start_page:end_page])

def get_location_id(city: str) -> int | None:
    location = read_query('SELECT id FROM locations WHERE city=?', (city,))
    if location:
        return int(location[0][0])
    return None

def get_qualifications_test(jad_id: int, qualifications:list, skills_count: int) -> QualificationLevel | None:
    data = read_query(
        '''Select qualifications_id, levels_of_qualification_id FROM qualifications_levels WHERE job_ads_id= ?''',
        (jad_id,))

    counter = 0
    for tuple in data:
        q = tuple[0]
        l = tuple[1]
        for skills in qualifications:
            skill, level = skills.split(':')
            skill = int(skill)
            level = int(level)
            if skill == q and l >= level:
                counter += 1
                    
    if counter < skills_count:
        data = []
    
    if data == []:
        return None
    
    else:
        return [QualificationLevel.from_query_result(*row) for row in data]

def sort(job_ads: list[Job_ad], *, attribute='minimum_salary', reverse=False):
    if attribute == 'minimum_salary':
        def sort_fn(ja: Job_ad): return ja.minimum_salary
    elif attribute == 'maximum_salary':
        def sort_fn(ja: Job_ad): return ja.maximum_salary
    elif attribute == 'location':
        def sort_fn(ja: Job_ad): return ja.location_id
    else:
        def sort_fn(ja: Job_ad): return ja.id

    return sorted(job_ads, key=sort_fn, reverse=reverse)

def ad_exist(id:int):
    data = read_query_single("SELECT id from job_ads where id= ?", (id,))
    if data is None:
        return False
    return True


def update_qualification_ids(job_ads_id: int, qualification_levels:list[QualificationLevel]):
    relations = ','.join(
        f'({QualificationLevel.qualifications_id},{QualificationLevel.levels_of_qualification_id}, {job_ads_id})' for  QualificationLevel in qualification_levels)
    insert_query(
        f'Insert into qualifications_levels(qualifications_id,levels_of_qualification_id, job_ads_id) VALUES {relations} ON DUPLICATE KEY UPDATE qualifications_id = VALUES(qualifications_id), levels_of_qualification_id = VALUES(levels_of_qualification_id)')

def update_qualifications(job_ads_id: int,qualification_levels:list[QualificationLevels]):
    for element in qualification_levels:
        if qualification_exist(element.qualification) is False:
            add_new_qualifications(element.qualification)
        if level_exist(element.level) is False:
            return 'No such level'
    
    for element in qualification_levels: 
        data = read_query('SELECT qualifications.id,levels_of_qualification.id from qualifications cross join levels_of_qualification where qualifications.qualification = ? and levels_of_qualification.level = ?',(element.qualification,element.level))
        result = [QualificationLevel.from_query_result(*row) for row in data]  
    update_qualification_ids(job_ads_id, result) 
    new = get_by_id(job_ads_id)  

    return new  
