from common.auth import _hash_password, find_by_username
from common.helpers import send_mail
from common.responses import BadRequest, Forbidden, Success
from common.validators import get_location_by_id, location_exists
from data.database import delete_query, read_query, insert_query, read_query_single, update_query
from data.models import Company_ad, Company_adResponse, Contacts, ContactsResponseModel, Job_ad, Location, Professional, ProfessionalInfo, ProfessionalResponseModel, QualificationLevel, TPassword
from mariadb import IntegrityError
from services import job_ad_service
from common import helpers, validators
from services.employer_service import get_employer_by_id


def create(professional: Professional, location: Location, contacts: Contacts, insert_data_func: None) -> Professional | IntegrityError:
    if insert_data_func == None:
        insert_data_func = insert_query
    password = _hash_password(professional.password)
    location_is_valid = location_exists(location.city)

    if not location_is_valid:
        return BadRequest(f'No city {location.city}') 
    
    location.id = validators.get_location_id_by_city(location.city)

    contacts_id = insert_data_func(
        'INSERT INTO contacts(phone, e_mail, twitter, address) VALUES (?,?,?,?)',
        (contacts.phone, contacts.e_mail,contacts.twitter, contacts.address))

    try:  
              
        if professional.photo:
            professional.photo =  helpers.upload_photo(professional.photo, professional.username)

        generated_id = insert_data_func(
            'INSERT INTO professionals(username, password, first_name, last_name, \
                summary, is_busy, foto, Location_id, Contacts_id) VALUES (?,?,?,?,?,?,?,?,?)',
            (professional.username, password, professional.first_name, professional.last_name,\
                professional.summary, False, professional.photo, location.id, contacts_id))

      
        return Professional(id=generated_id, username=professional.username, password=professional.password,\
            first_name=professional.first_name, last_name=professional.last_name, summary=professional.summary, \
                photo=professional.photo, is_busy=False, location=location.id, contacts=contacts_id)

    except IntegrityError:
        delete_query('DELETE FROM contacts WHERE id=?', (contacts_id,))

        # print(IntegrityError) # mariadb raises this error when a constraint is violated
        
        return None


def login(professional_username: str, password: str) -> Professional | None:
    professional = find_by_username(professional_username, 'professionals')
    password = _hash_password(password)
    return professional if professional and professional.password == password else None


def get_contacts(id:int, get_data_func=None) -> Contacts:
    if get_data_func == None:
        get_data_func = read_query
    data = get_data_func('SELECT id, phone, e_mail, twitter, address FROM contacts WHERE id=?', (id,))
    return next((Contacts(id=id, phone=phone, e_mail=e_mail, twitter=twitter, address=address) for id, phone, e_mail, twitter, address in data), None)


def update_info(professional: Professional|None, current_professional: Professional| None, \
    location:Location|None, new_contacts: Contacts | None):
    if location:
        is_valid_location = location_exists(location.city)
        if not is_valid_location:
            return BadRequest("Invalid Location")
        new_location = validators.get_location_id_by_city(location.city)
            
    if professional and professional.photo:
        new_photo = helpers.upload_photo(professional.photo, current_professional.username)

    if new_contacts:
        current_contacts = get_contacts(current_professional.contacts)
        updated_contacts = Contacts(
            phone=new_contacts.phone or current_contacts.phone,
            e_mail=new_contacts.e_mail or current_contacts.e_mail,
            twitter=new_contacts.twitter or current_contacts.twitter,
            address=new_contacts.address or current_contacts.address
        )
        updated_contacts_data = update_query(
            'UPDATE contacts SET phone=?, e_mail=?, twitter=?, address=? \
            WHERE id=?',(updated_contacts.phone, updated_contacts.e_mail,
            updated_contacts.twitter, updated_contacts.address, current_professional.contacts)
        )

    updated_professional = Professional(
        id = current_professional.id,
        username=current_professional.username,
        password=current_professional.password,
        first_name= professional.first_name or current_professional.first_name \
            if professional else current_professional.first_name,
        last_name= professional.last_name or current_professional.last_name \
            if professional else current_professional.last_name,
        summary=professional.summary or current_professional.summary\
            if professional else current_professional.summary,
        is_busy=professional.is_busy or current_professional.is_busy\
            if professional else current_professional.is_busy,
        photo = new_photo if professional and professional.photo else current_professional.photo,
        location = new_location if location else current_professional.location,
        contacts= current_professional.contacts
    )
    updated_data = update_query('UPDATE professionals SET \
        first_name=?, last_name=?,\
        summary=?, is_busy=?, foto=?, location_id=? WHERE id=?',\
            (updated_professional.first_name, updated_professional.last_name, \
                updated_professional.summary, updated_professional.is_busy, \
                    updated_professional.photo, updated_professional.location, updated_professional.id))
    
    return ProfessionalResponseModel(username=updated_professional.username,
        first_name=updated_professional.first_name,
        last_name=updated_professional.last_name, 
        summary=updated_professional.summary,
        is_busy= 'Busy' if updated_professional.is_busy == 1 else 'Active', 
        photo= updated_professional.photo,
        location=get_location_by_id(updated_professional.location),
        contacts=get_contacts(updated_professional.contacts))


def update_password(current_pass:TPassword, new_pass: TPassword, professional: Professional):
    new_pass_hash = _hash_password(new_pass)
    result = update_query(
        '''UPDATE professionals SET
            password = ?
            WHERE id = ?
        ''',
        (new_pass_hash, professional.id))
    return result

def get_ads(id: int, status):
    if status is not None:
            status = status.lower()
            status_id = read_query_single(f"Select id from job_match.status where status =?", (status,))
            if status_id is None:
                return BadRequest(f'No status {status}')

            data = read_query('''SELECT id, minimum_salary, maximum_salary, description,\
                is_full_remote, is_partial_remote, is_main_ad, status_id, professionals_id, location_id
                FROM company_ads AS cad LEFT JOIN professional_qualifications_levels AS pql
                ON cad.id = pql.company_ads_id WHERE cad.professionals_id =? and cad.status_id =?''', (id,status_id[0]) )
    else:
        data = read_query('''SELECT id, minimum_salary, maximum_salary, description,\
                is_full_remote, is_partial_remote, is_main_ad, status_id, professionals_id, location_id
                FROM company_ads AS cad LEFT JOIN professional_qualifications_levels AS pql
                ON cad.id = pql.company_ads_id WHERE cad.professionals_id =?''', (id,))
        
    flattened = {}

    for id, minimum_salary, maximum_salary, description, is_full_remote, \
        is_partial_remote, is_main_ad, status_id, professionals_id, location_id, in data:

        if id not in flattened:
            flattened[id] = (
            id, minimum_salary, maximum_salary, description, is_full_remote, is_partial_remote, is_main_ad, 
            status_id, professionals_id, location_id, [], [])
            employers_ids = get_employers(id)
            flattened[id][-2].append(employers_ids)
            qualifications_ids = get_qualifications(id)
            flattened[id][-1].append(qualifications_ids)

    return (Company_adResponse.from_query_result(*obj) for obj in flattened.values())


def get_all(location):
    base_query = 'SELECT id, first_name, last_name, summary, location_id, is_busy, foto FROM professionals'
    if location:
        location_id = validators.get_location_id_by_city(location)
        base_query += (' WHERE location_id=?')
        sql_params = (location_id,)
        
    data = read_query(base_query, sql_params if location else None)

    flattened = {}

    for id, first_name, last_name, summary, location_id, is_busy, foto in data:
        location = get_location_by_id(location_id)
        status = 'Busy' if is_busy==1 else 'Active'
        active_ads = 0 if is_busy == 0 else get_active_ads_number(id)
        if id not in flattened:      
            flattened[id] = (first_name, last_name, summary, location, status, foto, active_ads, [])            
            matches_tup = get_matches(id)
            matches = [x[0] for x in matches_tup]
            flattened[id][-1].append(matches)
   
    return (ProfessionalInfo.from_query_result(*obj) for obj in flattened.values())

   
def get_qualifications(id: int) -> list[QualificationLevel]:
    data = read_query(
        '''SELECT qualifications_id, levels_of_qualification_id FROM professional_qualifications_levels WHERE company_ads_id= ?''',
        (id,))

    return [QualificationLevel.from_query_result(*row) for row in data]

def request_match(professional: Professional,jad_id: int):
    match_request_exists = read_query('SELECT * from jad_match_requests \
        WHERE professionalS_id=? AND job_ads_id=?', (professional.id, jad_id))
    job_status = job_ad_service.get_job_ad_status(jad_id)

    if job_status.lower() != 'active':
        return BadRequest('Match requests can be sent only to active job ads')

    if match_request_exists:
        return BadRequest(f'Match already requested for Job Ad with id:{jad_id}')

    match_request = insert_query('INSERT INTO jad_match_requests(professionals_id, job_ads_id) \
      VALUES (?, ?)', (professional.id, jad_id))

    job_ad = job_ad_service.get_by_id(jad_id)
    employer = get_employer_by_id(job_ad.employers_id)
    return send_mail(employer.employer_name, f"{professional.first_name} {professional.last_name}", jad_id)

def match(professional: Professional, ad_id:int, employer_id:int):

    professional_is_owner = check_if_owner(professional, ad_id)

    if not professional_is_owner:
        return Forbidden('Only company ad owner can match a request!')
    
    else:   
        match_request = read_query('SELECT is_matched FROM cad_match_requests \
            WHERE company_ads_id=? AND employers_id=?', (ad_id, employer_id))
        if not match_request or match_request[0] == (1,):
            return BadRequest('No such match request or ad is already matched!')
        update_professional = set_to_busy(professional)
        change_current_ad_status = change_company_ad_status([(ad_id,)], 5)
        other_ads = read_query('SELECT id FROM company_ads \
            WHERE professionals_id=? AND status_id != 5', (professional.id,))
        change_other_ads_status = change_company_ad_status(other_ads, 3)
        update_match = update_query('UPDATE cad_match_requests SET is_matched=? \
            WHERE company_ads_id=? AND employers_id=?', (True, ad_id, employer_id))
        return Success('Congrats on your new job!')
    
def check_if_owner(professional: Professional, ad_id:int):
    owner = read_query_single('SELECT id, professionals_id FROM company_ads \
        WHERE id=? AND professionals_id=?',(ad_id, professional.id)) 
    if not owner:
        return False
    return True

def set_to_busy(professional:Professional):
    update_professional = update_query('UPDATE professionals SET is_busy=?\
        WHERE id=?', (True, professional.id,))

def change_company_ad_status(ads_id: list, status:int):
    if len(ads_id) > 1:
        ads_id= [x[0] for x in ads_id]
    else:
        ads_id = ads_id[0]
    try:
        for id in ads_id:
            update_query('UPDATE company_ads \
                SET status_id=? WHERE id=?', (status, id))
        return True
    except IntegrityError:
        return False

def get_active_ads_number(id):
    data = read_query('SELECT * FROM company_ads WHERE professionals_id=? and status_id=?',(id, 1))
    active_ads_count = len(data)
    return active_ads_count

def get_matches(id):

    cads_ids = read_query('SELECT id FROM company_ads WHERE professionals_id=?', (id,))
    cads_tup = tuple(str(tup[0]) for tup in cads_ids)
    cads = ','.join(cads_tup)

    sql_params = (cads, 1)

    cads_data = read_query('SELECT employer_name FROM cad_match_requests as cmr \
	    RIGHT JOIN employers as e ON cmr.Employers_id=e.id\
	    WHERE cmr.Company_ads_id IN (?) AND cmr.is_matched=?', sql_params)
    
    jads_data = read_query('SELECT employer_name FROM jad_match_requests as jmr \
            RIGHT JOIN job_ads as ja ON jmr.Job_ads_id=ja.id \
                RIGHT JOIN employers as e ON ja.Employers_id=e.id\
                    WHERE jmr.professionals_id=? AND jmr.is_matched=?', (id, 1))
    
    employers = []
    for employer in cads_data:
        employers.append(employer)
    for employer in jads_data:
        employers.append(employer)

    return employers

def get_employers(cad_id):
    employers_ids = read_query('SELECT employer_name FROM cad_match_requests as cmr \
	    RIGHT JOIN employers as e ON cmr.Employers_id=e.id\
	    WHERE cmr.Company_ads_id=?', (cad_id,))


def get_professional_by_id(id, get_data_func= None) -> Professional| None:
    if get_data_func is None:
        get_data_func = read_query
    data = get_data_func('SELECT * FROM professionals WHERE id=?', (id,))
    if data is None:
        return None
    return next(Professional.from_query_result(*row) for row in data)

def delete_contacts(id):
        delete_query('DELETE * FROM contacts WHERE id=?', (id,))

def get_professionals_names(ids: list = None):
    professionals_names = []
    for id in ids:
        professional = get_professional_by_id(id)
        professionals_names.append(f"{professional.first_name} {professional.last_name}")
    return professionals_names
