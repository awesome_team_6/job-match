from common import helpers
from common.auth import _hash_password, find_by_username
from common.responses import BadRequest, Success
from common.validators import get_location_id_by_city, location_exists
from data.database import delete_query, insert_query, read_query, update_query, read_query_single
from data.models import Employer, Contacts, Employer_Response, TPassword, Job_adResponse, Company_ad, Match, \
    Professional
from mariadb import IntegrityError
from services import job_ad_service
from services.job_ad_service import get_qualifications
# from services.professional_service import get_professional_by_id


def create_employer_response(employer, get_data_func = None):
    if get_data_func is None:
        get_data_func = read_query
    data = get_data_func(
    "SELECT e.id, e.username, e.employer_name, e.description, e.logo, l.city, c.phone, c.e_mail, c.twitter, c.address, "
    "(select COUNT(id) FROM job_ads where Employers_id = e.id and status_id =1), "
    "(select COUNT(Company_ads_id)  FROM cad_match_requests where Employers_id = e.id) "
    "FROM employers as e join locations as l on e.Location_id = l.id join contacts as c on e.Contacts_id = c.id"
    " WHERE e.id = ?",
        (employer.id,))
    return next((Employer_Response.from_query_result(*row) for row in data), None)

def try_login(username: str, password: str) -> Employer | None:
    user = find_by_username(username, 'employers')
    password = _hash_password(password)
    return user if user and user.password == password else None

def create(username: str, password: str, employer_name: str, description: str, logo: str,
           phone:str, e_mail:str,twitter:str, address:str, city:str,
           insert_data_func = None, read_single_data_func = None, check_location= None, upload_logo = None) -> Employer | str | None:

    if read_single_data_func is None:
        read_single_data_func = read_query_single

    if insert_data_func is None:
        insert_data_func = insert_query

    if check_location is None:
        check_location = location_exists

    if upload_logo is None:
        upload_logo = helpers.upload_photo

    password = _hash_password(password)

    if check_location(city) is False:
        return "No location"

    location_id = read_single_data_func("select id from Locations where city = ?", (city,))[0]

    try:
        generated_contact_id = insert_data_func(
            'INSERT INTO contacts(phone, e_mail, twitter, address) VALUES (?,?,?,?)',
            (phone, e_mail,twitter, address))

        generated_e_id = insert_data_func(
            'INSERT INTO employers(username, password, employer_name, description, logo,location_id, contacts_id) VALUES (?,?,?,?,?,?,?)',
            (username, password, employer_name, description, logo, location_id, generated_contact_id))

        if logo is not None:
            logo = upload_logo(logo, employer_name)

        return Employer(id=generated_e_id, username=username, password=password, employer_name =employer_name,
                        description = description, logo = logo, location_id= location_id, contacts_id = generated_contact_id)

    except IntegrityError:
        delete_query('DELETE FROM contacts WHERE id=?', (generated_contact_id,))
        # mariadb raises this error when a constraint is violated
        # in that case we have duplicate usernames but have to delete already created contact
        return None

def update_employer(contacts: Contacts,new_contact:Contacts, city, employer, new_employer):


    new_employer.logo = helpers.upload_photo(new_employer.logo or employer.logo, new_employer.employer_name or employer.employer_name)

    employer_merged = Employer(id=employer.id,
            username = new_employer.username or employer.username,
            password = employer.password,
            employer_name = new_employer.employer_name or employer.employer_name,
            description = new_employer.description or employer.description,
            logo = new_employer.logo or employer.logo,
            location_id = new_employer.location_id or employer.location_id,
            contacts_id = employer.contacts_id
            )


    update_query(
        '''UPDATE employers SET
            username = ?, employer_name = ?, description = ?, logo = ?, location_id = ?
            WHERE id = ?
        ''',
        (employer_merged.username, employer_merged.employer_name, employer_merged.description,
         employer_merged.logo,get_location_id_by_city(city), employer_merged.id))


    contact_merged = Contacts(id=contacts.id,
            phone = new_contact.phone or contacts.phone,
            e_mail =new_contact.e_mail or contacts.e_mail,
            twitter = new_contact.twitter or contacts.twitter,
            address = new_contact.address or contacts.address
            )


    update_query(
        '''UPDATE Contacts SET
            phone = ?, e_mail = ?, twitter = ?, address = ?
            WHERE id = ?
        ''',
        (contact_merged.phone, contact_merged.e_mail, contact_merged.twitter, contact_merged.address, contact_merged.id))
    print(employer_merged.logo)
    return employer_merged

def update_employer_password(current_password:TPassword, new_password: TPassword, employer: Employer) -> TPassword | None:

    if _hash_password(current_password) == employer.password:
        new_password = _hash_password(new_password)
        update_query(
            '''UPDATE Employers SET
                password = ?
                WHERE id = ?
            ''',
            (new_password, employer.id))

        return new_password

    return None


def get_employer_ads(employer_id: int, jad_status):

        if jad_status is not None:
            status_id = read_query_single(f"Select id from job_match.status where status =?", (jad_status,))
            if status_id is None:
                return []

            data = read_query('''SELECT id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id
                FROM job_ads AS ja LEFT JOIN qualifications_levels AS ql
                ON ja.id = ql.job_ads_id where ja.employers_id =? and ja.status_id =?''', (employer_id,status_id[0]) )
        else:
            data = read_query('''SELECT id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id
            FROM job_ads AS ja LEFT JOIN qualifications_levels AS ql
            ON ja.id = ql.job_ads_id where ja.employers_id =?''',  (employer_id,)
            )

        flattened = {}

        for id, minimum_salary, maximum_salary, description, is_full_remote, is_partial_remote, location_id, status_id, employers_id in data:

            if id not in flattened:
                flattened[id] = (
                id, minimum_salary, maximum_salary, description, is_full_remote, is_partial_remote, location_id,
                status_id, employers_id, [])
                qualifications_ids = get_qualifications(id)
                flattened[id][-1].append(qualifications_ids)

        return (Job_adResponse.from_query_result(*obj) for obj in flattened.values())


def all(search_by_name: str = None, search_by_location: str = None, get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func(
        "SELECT e.id, e.username, e.employer_name, e.description,e.logo, l.city, c.phone, c.e_mail, c.twitter, c.address, "
        "(select COUNT(id) FROM job_ads where Employers_id = e.id and status_id =1), ((select COUNT(Company_ads_id)  FROM cad_match_requests where Employers_id = e.id and is_matched = 1)"
        " + (select COUNT(job_ads_id)  FROM jad_match_requests where Job_ads_id in (select id FROM job_ads where Employers_id = e.id) and is_matched = 1))"
        "FROM employers as e join locations as l on e.Location_id = l.id join contacts as c on e.Contacts_id = c.id")

    result = (Employer_Response.from_query_result(*row) for row in data)

    if search_by_name is None and search_by_location is not None:
        return (employer for employer in result if search_by_location.lower() in employer.location.lower())
    elif search_by_name is not None and search_by_location is None:
        return (employer for employer in result if search_by_name.lower() in employer.employer_name.lower())
    elif search_by_name is not None and search_by_location is not None:
        return (employer for employer in result if search_by_name.lower() in employer.employer_name.lower()
                and search_by_location.lower() in employer.location.lower())

    return result


def match_requests_in_job_ad(employer:Employer, match: Match, update_data_func=None):
    if update_data_func == None:
        update_data_func = update_query

    for user in match.user_ids:

        update_data_func(
        '''UPDATE jad_match_requests SET
            is_matched = ?
            WHERE Professionals_id = ? and Job_ads_id = ? 
        ''',
        (1, user, match.ad_id))

        update_data_func(
        '''UPDATE professionals SET
            is_busy = ?
            WHERE id = ? 
        ''',
        (1, user))

    if change_job_ad_status(match.ad_id, status=2) == True:
        return Success('Congrats on your new match!')
    return BadRequest


def send_match_request_to_company_ad(company_ad:Company_ad, employer: Employer, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = insert_query
    try:
        insert_data_func(
            'INSERT INTO cad_match_requests(company_ads_id, employers_id) VALUES (?,?)',
            (company_ad.id, employer.id))
        return employer
    except IntegrityError:
        return None

def check_if_owner(employer: Employer, ad_id:int) -> bool:
    owner = read_query_single('SELECT id, Employers_id FROM job_ads \
        WHERE id=? AND Employers_id=?',(ad_id, employer.id))
    if not owner:
        return False
    return True

def change_job_ad_status(ads_id: int, status:int):
    try:
        update_query('UPDATE job_ads SET status_id=? where id =?', (status, ads_id))
        return True
    except IntegrityError:
        return False

def check_if_ad_is_valid(ad_id):
    if job_ad_service.get_by_id(ad_id) is None or job_ad_service.get_job_ad_status(ad_id) != "active":
        return False
    return True

def check_if_professional_is_valid(id: int):
    data = read_query_single('SELECT * FROM professionals WHERE id=? and is_busy =?', (id,0))
    if data is None:
        return False
    return True

def job_match_exists(ad_id, professional_id):
    data = read_query_single('SELECT * FROM jad_match_requests WHERE Job_ads_id=? and Professionals_id =?', (ad_id,professional_id))
    if data is None:
        return False
    return True

def get_employer_by_id(id, get_data_func = None) -> Employer| None:
    if get_data_func is None:
        get_data_func = read_query
    data = get_data_func('SELECT * FROM employers WHERE id=?', (id,))
    if data is None:
        return None
    return next((Employer.from_query_result(*row) for row in data))



# def get_jad_qualification_and_level_requirements(jad_id: int) -> list[Qualification_and_Level_requirements]:
#
#     data = read_query(
#                     'SELECT q.qualification, lq.level FROM levels_of_qualification as lq'
#                     ' join qualifications_levels as ql on ql.levels_of_qualification_id = lq.id '
#                     'join qualifications as q on ql.Qualifications_id = q.id WHERE ql.Job_ads_id=?', (jad_id,))
#
#     qualification_and_level_requirements = [Qualification_and_Level_requirements.from_query_result(*row) for row in data]
#     return qualification_and_level_requirements
#
#
# def get_employer_ads(id: int) -> list[Job_ad_respond_model]:
#     data = read_query('SELECT j.id, j.minimum_salary, j.maximum_salary, j.description, j.is_full_remote, j.is_partial_remote,'
#                       ' l.city, j.status_id, e.employer_name FROM job_ads as j '
#                       'join locations as l on j.Location_id = l.id join employers as e on j.employers_id = e.id '
#                       'WHERE j.employers_id=?',(id,))
#
#     list_of_employer_adds = []
#     ads = (Job_ad_respond_model.from_query_result(*row) for row in data)
#     for ad in ads:
#         ad.qualification_and_level_requirements = get_jad_qualification_and_level_requirements(ad.id)
#         list_of_employer_adds.append(ad)
#
#
#     return list_of_employer_adds

