from common.responses import BadRequest
from data.database import read_query, insert_query, update_query, read_query_single
from data.models import Company_ad, New_Company_Ad, Professional, QualificationLevel, Professional, Company_adResponse, QualificationLevels, Company_Response, CompanyResponseModel

def create(company_ad: Company_Response, professional: Professional):

    company_ad.professionals_id = professional.id
    generated_id = insert_query(
        'INSERT INTO company_ads(minimum_salary, maximum_salary, description, is_full_remote,is_partial_remote, is_main_ad, status_id,professionals_id, location_id) VALUES(?,?,?,?,?,?,?,?,?)',
        (company_ad.minimum_salary,  company_ad.maximum_salary, company_ad.description, company_ad.is_full_remote, company_ad.is_partial_remote, company_ad.is_main_ad, company_ad.status_id, company_ad.professionals_id, company_ad.location_id))
   
    company_ad.id = generated_id

    add_qualification_levels(company_ad.id,company_ad.qualification_levels)

    return company_ad

def get_match_requests(company_ads_id: int):
    data = read_query(
        '''Select employer_name FROM employers join cad_match_requests WHERE employers.id = employers_id and company_ads_id= ?''',
        (company_ads_id,))

    return  [i[0] for i in data]
    
    
def insert_qualifications(company_ads_id: int, qualification_levels:list[QualificationLevel]):
    relations = ','.join(
        f'({QualificationLevel.qualifications_id},{QualificationLevel.levels_of_qualification_id}, {company_ads_id})' for  QualificationLevel in qualification_levels)
    insert_query(
        f'INSERT INTO professional_qualifications_levels(qualifications_id,levels_of_qualification_id, company_ads_id) VALUES {relations}')
        

def add_qualification_levels(company_ads_id: int, qualification_levels:list[QualificationLevels]):
    
    for element in qualification_levels:
        if qualification_exist(element.qualification) is False:
            add_new_qualifications(element.qualification)
        if level_exist(element.level) is False:
            return 'No such level'
    
    for element in qualification_levels: 
        data = read_query('SELECT qualifications.id,levels_of_qualification.id from qualifications cross join levels_of_qualification where qualifications.qualification = ? and levels_of_qualification.level = ?',(element.qualification,element.level))
        result = [QualificationLevel.from_query_result(*row) for row in data]       
        insert_qualifications(company_ads_id,result)
    

def add_new_qualifications(qualification: str):
    generated_id = insert_query('INSERT INTO qualifications(qualification) VALUES(?)', (qualification,))

def get_qualifications(company_ads_id: int) -> list[QualificationLevel]:
    data = read_query(
        '''Select qualifications_id, levels_of_qualification_id FROM professional_qualifications_levels WHERE company_ads_id= ?''',
        (company_ads_id,))

    return [QualificationLevel.from_query_result(*row) for row in data]

def qualification_exist(qualification):
    data = read_query_single("SELECT id from qualifications where qualification = ?", (qualification,))
    if data is None:
        return False
    return True

def level_exist(level):
    data = read_query_single("SELECT id from levels_of_qualification where level = ?", (level,))
    if data is None:
        return False
    return True

def create_response_object(company_ad: Company_ad, professional: Professional, professional_qualifications_levels: list[QualificationLevel]):

    return {
        'id': company_ad.id,
        'minimum_salary':company_ad.minimum_salary,
        'maximum_salary':company_ad.maximum_salary,
        'description': company_ad.description,
        'is_full_remote': company_ad.is_full_remote,
        'is_partial_remote': company_ad.is_partial_remote, 
        'is_main_ad': company_ad.is_main_ad,
        'status_id': company_ad.status_id,
        'professionals_id': company_ad.professionals_id,
        'location_id': company_ad.location_id,
        'qualifications_ids': professional_qualifications_levels
    }

def sort(company_ads: list[Company_ad], *, attribute='minimum_salary', reverse=False):
    if attribute == 'minimum_salary':
        def sort_fn(ca: Company_ad): return ca.minimum_salary
    elif attribute == 'maximum_salary':
        def sort_fn(ca: Company_ad): return ca.maximum_salary
    elif attribute == 'location':
        def sort_fn(ca: Company_ad): return ca.location_id
    else:
        def sort_fn(ca: Company_ad): return ca.id

    return sorted(company_ads, key=sort_fn, reverse=reverse)

def get_by_id(id: int):
    data = read_query('SELECT id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id FROM company_ads where id = ?', (id,))
    flattened = {}
    
    for id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id in data:
        
        if id not in flattened:
            
            flattened[id] = (id,minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id,[],[])
            employers_ids = get_match_requests(id)
            flattened[id][-2].append(employers_ids)
            qualifications_ids = get_qualifications(id)
            flattened[id][-1].append(qualifications_ids)
            

    return next((Company_adResponse.from_query_result(*obj) for obj in flattened.values()), None)

def get_by_id_public(id: int):
    data = read_query('SELECT id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id FROM company_ads where id = ?', (id,))
    flattened = {}
    
    for id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id in data:
        
        if id not in flattened:
            
            flattened[id] = (id,minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id,[])
            qualifications_ids = get_qualifications(id)
            flattened[id][-1].append(qualifications_ids)
            

    return next((CompanyResponseModel.from_query_result(*obj) for obj in flattened.values()), None)

def get_by_id_model(id: int):
    
    data = read_query('SELECT id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,location_id FROM company_ads where id = ?', (id,))

    return next((New_Company_Ad.from_query_result(*obj) for obj in data), None)  

def qualification_exists(id: int):
    data = read_query('SELECT 1 from qualifications where id = ?', (id,))

    return any(data)

def update(old: Company_ad, new: Company_ad):

    merged = Company_ad(
        id = old.id,
        minimum_salary = new.minimum_salary or old.minimum_salary,
        maximum_salary = new.maximum_salary or old.maximum_salary,
        description = new.description or old.description,
        is_full_remote = new.is_full_remote or old.is_full_remote,
        is_partial_remote = new.is_partial_remote or old.is_partial_remote,
        is_main_ad = new.is_main_ad or old.is_main_ad,
        status_id = new.status_id or old.status_id,
        location_id = new.location_id or old.location_id)

    update_query(
        '''UPDATE company_ads SET
           minimum_salary=?, maximum_salary=?, description=?, is_full_remote=?,  is_partial_remote=?,  is_main_ad=?,  status_id=?, location_id=?
           WHERE id = ?
        ''',
        (merged.minimum_salary, merged.maximum_salary, merged.description, merged.is_full_remote,  merged.is_partial_remote,  merged.is_main_ad,  merged.status_id, merged.location_id, merged.id))

    return merged

def get_company_ad_status(id:int):
    data = read_query_single('''SELECT status FROM status JOIN company_ads ON status.id = company_ads.status_id where company_ads.id=?''', (id,))[0]
    status = data
    print(status)
    return status

def get_all(location, salary_min, salary_max, salary_threshold, skills_str, skills_threshold, page_number: int | None = None, page_size: int | None = None):
    
    if page_number is None:
        page_number = 1
    if page_size is None:
        page_size = 10
    page_number = int(page_number)
    page_size = int(page_size)
    start_page = (page_number-1) * page_size
    end_page = start_page + page_size
    
    base_query = 'SELECT id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id, professionals_id, location_id FROM company_ads as ca LEFT JOIN professional_qualifications_levels AS pql ON ca.id = pql.company_ads_id where status_id = 1'
      
 
    sql_args = []
    if not location and not salary_min and not salary_max and not skills_str:    
        data = read_query('''SELECT id, minimum_salary, maximum_salary, description,is_full_remote,\
        is_partial_remote,is_main_ad,status_id, professionals_id, location_id FROM company_ads as ca LEFT JOIN professional_qualifications_levels AS pql ON ca.id = pql.company_ads_id where status_id = 1
            ''')

    if location:
        location_id= get_location_id(location)
        if location_id is None:
            return BadRequest("Please select valid location")
        base_query += ' AND (location_id=? OR is_full_remote=?)'
        sql_args.append(location_id)
        sql_args.append(1)

    if salary_min or salary_max:
        if salary_threshold:
            salary_min = int(salary_min - salary_min * (salary_threshold/100))
            salary_max = int(salary_max + salary_max * (salary_threshold/100))
        if location: 
            base_query += ' AND ((minimum_salary BETWEEN ? AND ? OR  maximum_salary BETWEEN ? AND ?) OR (minimum_salary<= ? AND maximum_salary>= ?))'
        else: 
            base_query += ' AND ((minimum_salary BETWEEN ? AND ? OR  maximum_salary BETWEEN ? AND ?) OR (minimum_salary<= ? AND maximum_salary>= ?))'
        sql_args.append(salary_min)
        sql_args.append(salary_max)
        sql_args.append(salary_min)
        sql_args.append(salary_max)
        sql_args.append(salary_min)
        sql_args.append(salary_max)

    if skills_str:
        skills = skills_str.split(', ')
        skills_count = len(skills)
        if skills_threshold:
            skills_count -= skills_threshold 

    data = read_query(base_query, tuple(sql_args))
    flattened = {}
    to_remove = []
    
    for id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id in data:
        
        if id not in flattened:
            
            flattened[id] = (id,minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id,[])
            if skills_str:
                qualifications_ids = get_qualifications_test(id, skills, skills_count)
                if not qualifications_ids: 
                    to_remove.append(id)      
            else:
                qualifications_ids = get_qualifications(id)
            flattened[id][-1].append(qualifications_ids)
        
    for id in to_remove:
        del flattened[id]
    
    return (CompanyResponseModel.from_query_result(*obj) for obj in list(flattened.values())[start_page:end_page])

def get_location_id(city: str) -> int | None:
    location = read_query('SELECT id FROM locations WHERE city=?', (city,))
    if location:
        return int(location[0][0])
    return None

def get_qualifications_test(cad_id: int, qualifications:list, skills_count: int) -> QualificationLevel | None:
    data = read_query(
        '''Select qualifications_id, levels_of_qualification_id FROM professional_qualifications_levels WHERE company_ads_id= ?''',
        (cad_id,))

    counter = 0
    for tuple in data:
        q = tuple[0]
        l = tuple[1]
        for skills in qualifications:
            skill, level = skills.split(':')
            skill = int(skill)
            level = int(level)
            if skill == q and l >= level:
                counter += 1
                    
    if counter < skills_count:
        data = []
    
    if data == []:
        return None
    
    else:
        return [QualificationLevel.from_query_result(*row) for row in data]

def ad_exist(id:int):
    data = read_query_single("SELECT id from company_ads where id= ?", (id,))
    if data is None:
        return False
    return True
        
     
def update_qualification_ids(company_ads_id: int, qualification_levels:list[QualificationLevel]):
    relations = ','.join(
        f'({QualificationLevel.qualifications_id},{QualificationLevel.levels_of_qualification_id}, {company_ads_id})' for  QualificationLevel in qualification_levels)
    insert_query(
        f'Insert into professional_qualifications_levels(qualifications_id,levels_of_qualification_id, company_ads_id) VALUES {relations} ON DUPLICATE KEY UPDATE qualifications_id = VALUES(qualifications_id), levels_of_qualification_id = VALUES(levels_of_qualification_id)')

def update_qualifications(company_ads_id: int,qualification_levels:list[QualificationLevels]):
    for element in qualification_levels:
        if qualification_exist(element.qualification) is False:
            add_new_qualifications(element.qualification)
        if level_exist(element.level) is False:
            return 'No such level'
    
    for element in qualification_levels: 
        data = read_query('SELECT qualifications.id,levels_of_qualification.id from qualifications cross join levels_of_qualification where qualifications.qualification = ? and levels_of_qualification.level = ?',(element.qualification,element.level))
        result = [QualificationLevel.from_query_result(*row) for row in data]  
    update_qualification_ids(company_ads_id, result) 
    new = get_by_id(company_ads_id)  

    return new 
