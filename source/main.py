from fastapi import FastAPI
from routers.employers import employers_router
from routers.professionals import professionals_router
from routers.job_ads import job_ads_router
from routers.company_ads import company_ads_router

app = FastAPI()
app.include_router(employers_router)
app.include_router(professionals_router)
app.include_router(job_ads_router)
app.include_router(company_ads_router)
