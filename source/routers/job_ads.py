from fastapi import APIRouter, Header, Query
from common.responses import BadRequest, Forbidden, NotFound, Success, Unauthorized
from data.models import Job_ad, Job_adResponse, New_Job_ad, JobResponseModel, QualificationLevels
from services import job_ad_service
from common.auth import get_user_or_raise_401
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi import HTTPException
from typing import Union
from common.validators import location_exists

job_ads_router = APIRouter(prefix='/jads')

@job_ads_router.get('/')
def get_jads(location: str|None=None,\
     salary_min: int|None=None, salary_max:int | None=None,\
        salary_threshold: int | None=None, \
            skills: str | None=None, skills_threshold:int | None=None,page_number: int | None = None,\
               page_size: int | None = None,
               sort: str | None = None,
               sort_by: str | None = None, x_token=Header()):
    professional = get_user_or_raise_401(x_token, "professionals")
    if location and location_exists(location) is False:
        return BadRequest(f'{location} is not available as a city in our app. Please try with real location.')
    ads = job_ad_service.get_all(location, salary_min, salary_max, salary_threshold, skills, skills_threshold, page_number, page_size)
    if sort and (sort == 'asc' or sort == 'desc'):
        return job_ad_service.sort(ads, reverse=sort == 'desc', attribute=sort_by)
    else:
        return  ads
    return ads

@job_ads_router.post('/')
def create_job_ad(job_ad: JobResponseModel, x_token: str = Header()):
    employer = get_user_or_raise_401(x_token, "employers")

    job_ad = job_ad_service.create(job_ad, employer)
    qualifications_ids = job_ad_service.get_qualifications(job_ad.id)

    return job_ad_service.create_response_object(job_ad, employer, qualifications_ids)

@job_ads_router.get('/{id}')
def get_job_ad_by_id(id: int, x_token: str = Header()):
    employer = get_user_or_raise_401(x_token, "employers")
    if not job_ad_service.ad_exist(id):
        return BadRequest(f'No ad with id {id}.')

    job_ad = job_ad_service.get_by_id(id)

    status = job_ad_service.get_job_ad_status(id)
   
    if status == 'active':
        return JSONResponse(jsonable_encoder(job_ad))

    if status == 'archived':
        return 'Ad no longer active'

@job_ads_router.patch('/{id}')
def update_job_ad(id:int, data: New_Job_ad, x_token: str = Header()):
    employer = get_user_or_raise_401(x_token, "employers")
    if not job_ad_service.ad_exist(id):
        return BadRequest(f'No ad with id {id}.')
    job_ad = job_ad_service.get_by_id(id)
    if not employer.id == job_ad.employers_id:
        return Forbidden('Not authorized')
    
    else:
        model_job_ad = job_ad_service.get_by_id_model(id)
        new_job_ad =  job_ad_service.update(model_job_ad, data) 
        updated_job_ad = job_ad_service.get_by_id(id)
        return JSONResponse(jsonable_encoder(updated_job_ad))


@job_ads_router.put('/{id}/q')
def update_job_ad(id:int, data:list[QualificationLevels], x_token: str = Header()):
    employer = get_user_or_raise_401(x_token, "employers")
    if not job_ad_service.ad_exist(id):
        return BadRequest(f'No ad with id {id}.')
    job_ad = job_ad_service.get_by_id(id)
    if not employer.id == job_ad.employers_id:
        return Forbidden('Not authorized')
    
    else:
        
        updated_job_ad =  job_ad_service.update_qualifications(id,data)
        
        return JSONResponse(jsonable_encoder(updated_job_ad))
