from fastapi import APIRouter, Header
from common.responses import BadRequest, Forbidden, NotFound, Success, Unauthorized
from common.validators import get_location_by_id
from data.models import Contacts, ContactsResponseModel, Location, LoginData, Match, Professional, ProfessionalResponseModel, NewPassword, ProfessionalInfo
from services import professional_service
from common.auth import get_user_or_raise_401, create_token


professionals_router = APIRouter(prefix='/professionals')

@professionals_router.post('/register')
def register(professional: Professional, location: Location, contacts: Contacts) -> Professional:
    
    new_professional = professional_service.create(professional, location, contacts, insert_data_func=None)

    return new_professional or BadRequest('Username taken or required field missing')


@professionals_router.post("/login")
def login(data: LoginData):
    professional = professional_service.login(data.username, data.password)

    if professional:
        token = create_token(professional)
        return {'token': token}
    else:
        return BadRequest('Invalid login data')

@professionals_router.get('/info')
def info(x_token: str=Header()):
    professional = get_user_or_raise_401(x_token, 'professionals')
    contacts = professional_service.get_contacts(professional.contacts)
    
    return ProfessionalResponseModel(
        username=professional.username,
        first_name=professional.first_name,
        last_name=professional.last_name, 
        summary=professional.summary,
        is_busy= 'Busy' if professional.is_busy == 1 else 'Active', 
        photo=professional.photo,
        location=get_location_by_id(professional.location),
        contacts=ContactsResponseModel(id=contacts.id, phone=contacts.phone, e_mail=contacts.e_mail, twitter=contacts.twitter, address=contacts.address)
    )

@professionals_router.patch('/info')
def update(professional: Professional | None=None, location: Location | None=None, \
    contacts: Contacts| None=None, x_token:str = Header()):
    current_professional = get_user_or_raise_401(x_token, 'professionals')
    updated_professional = professional_service.update_info(professional, current_professional, location, contacts)
    return updated_professional

@professionals_router.patch('/password')
def update_password(password_data: NewPassword, x_token:str = Header()):
    professional = get_user_or_raise_401(x_token, 'professionals')
    password_update = professional_service.update_password(password_data.current_pass, password_data.new_pass, professional)
    if not password_update:
        return BadRequest("You can't use same password")
    return Success("Password updated") 

@professionals_router.get('/ads')
def view_ads(status=None, x_token: str=Header()):
    current_professional = get_user_or_raise_401(x_token, 'professionals')
    ads = professional_service.get_ads(current_professional.id, status)
    
    return ads

@professionals_router.get('/all')
def view_professionals(location: str | None=None, x_token:str=Header()):
    person_requesting = get_user_or_raise_401(x_token, 'employers')
    professionals = professional_service.get_all(location)
    return professionals

@professionals_router.post('/matchrequest/{id}')
def request_match(id:int, x_token:str = Header()):
    professional = get_user_or_raise_401(x_token, 'professionals')
    print(professional)
    return professional_service.request_match(professional, id)

@professionals_router.post('/match')
def match(match: Match, x_token:str=Header()):
    person_requesting = get_user_or_raise_401(x_token, 'professionals')
    employer_id = int(match.user_ids[0])
    return professional_service.match(person_requesting, match.ad_id, employer_id)
    
