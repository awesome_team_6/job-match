from fastapi import APIRouter, Header
from common.responses import BadRequest, Forbidden, NotFound, Success, Unauthorized
from data.models import Company_ad, New_Company_Ad, Company_ad_Response, Company_Response, QualificationLevels
from services import company_ad_service
from common.auth import get_user_or_raise_401
from fastapi import HTTPException
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from common.validators import location_exists

company_ads_router = APIRouter(prefix='/cads')

@company_ads_router.get('/')
def get_cads(location: str|None=None,\
     salary_min: int|None=None, salary_max:int | None=None,\
        salary_threshold: int | None=None, \
            skills: str | None=None, skills_threshold:int | None=None,page_number: int | None = None,\
               page_size: int | None = None,
               sort: str | None = None,
               sort_by: str | None = None, x_token=Header()):
    
   
    employer = get_user_or_raise_401(x_token, "employers")

    if location and location_exists(location) is False:
        return BadRequest(f'{location} is not available as a city in our app. Please try with real location.')

    ads = company_ad_service.get_all(location, salary_min, salary_max, salary_threshold, skills, skills_threshold, page_number, page_size)
   
    if sort and (sort == 'asc' or sort == 'desc'):
        return company_ad_service.sort(ads, reverse=sort == 'desc', attribute=sort_by)
    else:
        return  ads

@company_ads_router.post('/', response_model=Company_ad_Response)
def create_company_ad(company_ad: Company_Response, x_token: str = Header()):
    professional = get_user_or_raise_401(x_token, "professionals")

    company_ad = company_ad_service.create(company_ad, professional)
    qualifications_ids = company_ad_service.get_qualifications(company_ad.id)

    company_ad_response = Company_ad_Response(id = company_ad.id, minimum_salary = company_ad.minimum_salary, maximum_salary=company_ad.maximum_salary,  
                      description = company_ad.description, is_full_remote = company_ad.is_full_remote, is_partial_remote= company_ad.is_partial_remote, status = company_ad_service.get_company_ad_status(company_ad.id), username = professional.username, location_id = company_ad.location_id, match_requests = [], qualifications_ids= qualifications_ids)
    
    return company_ad_response

@company_ads_router.get('/{id}')
def get_company_ad_by_id(id: int, x_token: str = Header()):
    professional = get_user_or_raise_401(x_token, "professionals")
    if not company_ad_service.ad_exist(id):
        return BadRequest(f'No ad with id {id}.')
    company_ad = company_ad_service.get_by_id(id)
    company_ad_private = company_ad_service.get_by_id_public(id) 
    status = company_ad_service.get_company_ad_status(id)

    if status == 'matched':
        return 'Ad no longer active'
    if status == 'active' or 'private':
        return JSONResponse(jsonable_encoder(company_ad_private))
    if status == 'hidden' and not professional.id == company_ad.professionals_id:
        return Forbidden
    if professional.id == company_ad.professionals_id:
        return JSONResponse(jsonable_encoder(company_ad))
    
    
    

@company_ads_router.patch('/{id}')
def update_company_ad(id:int, data: New_Company_Ad, x_token: str = Header()):
    professional = get_user_or_raise_401(x_token, "professionals")
    if not company_ad_service.ad_exist(id):
        return BadRequest(f'No ad with id {id}.')
    company_ad = company_ad_service.get_by_id(id)
    if not professional.id == company_ad.professionals_id:
        return Forbidden(('Not authorized'))
    
    
    
    else:
        model_company_ad = company_ad_service.get_by_id_model(id)
        new_company_ad =  company_ad_service.update(model_company_ad, data) 
        updated_company_ad = company_ad_service.get_by_id(id)
   
        return JSONResponse(jsonable_encoder(updated_company_ad))


@company_ads_router.put('/{id}/q')
def update_company_ad(id:int, data:list[QualificationLevels], x_token: str = Header()):
    professional = get_user_or_raise_401(x_token, "professionals")
    if not company_ad_service.ad_exist(id):
        return BadRequest(f'No ad with id {id}.')
    company_ad = company_ad_service.get_by_id(id)
    if not professional.id == company_ad.professionals_id:
        return Forbidden(('Not authorized'))
    
    
    else:
        
        updated_company_ad =  company_ad_service.update_qualifications(id,data)
        
        return JSONResponse(jsonable_encoder(updated_company_ad))
