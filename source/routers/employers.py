from fastapi import APIRouter, Header
from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse
from common.auth import get_user_or_raise_401
from common.helpers import send_mail
from common.responses import BadRequest, Forbidden, NotFound, Success, Unauthorized
from common.validators import location_exists, get_location_by_id
from data.models import Employer, LoginData, Location, Contacts, Employer_Response, TPassword, NewPassword, Match
from services import employer_service, company_ad_service
from common import auth, validators
from services.employer_service import create_employer_response, check_if_owner
from services.professional_service import get_professional_by_id, get_professionals_names

employers_router = APIRouter(prefix='/employers')

@employers_router.post('/register', response_model=Employer_Response)
def register(employer: Employer, location: Location ,contacts: Contacts):

    employer = employer_service.create(employer.username, employer.password, employer.employer_name, employer.description,
                                       employer.logo, contacts.phone, contacts.e_mail, contacts.twitter,
                                       contacts.address,location.city)

    if employer == "No location":
        return BadRequest(f'{location.city} is not available as a city in our app. Please try with real location.')


    if employer is None:
        return BadRequest(f'Username is taken.')

    employer_response = Employer_Response(id = employer.id, username = employer.username,  employer_name = employer.employer_name,
                      description = employer.description, logo=employer.logo, location = location.city, phone = contacts.phone,
                      e_mail = contacts.e_mail, twitter = contacts.twitter, address = contacts.address,
                      active_job_ads = 0, successful_matches =0)

    return employer_response

@employers_router.post("/login")
def login(data: LoginData):
    employer = employer_service.try_login(data.username, data.password)

    if employer:
        token = auth.create_token(employer)
        return {'token': token}
    else:
        return BadRequest('Invalid login data')

@employers_router.get('/info', response_model=Employer_Response)
def get_info(x_token: str = Header()):
     employer = get_user_or_raise_401(x_token, 'employers')
     employer_response = create_employer_response(employer)
     return employer_response or Forbidden(f'token is not valid')

@employers_router.put('/info')
def update_info(new_employer: Employer | None, new_contacts: Contacts | None, new_location: Location | None, x_token: str = Header()):
    employer = get_user_or_raise_401(x_token, 'employers')
    if location_exists(new_location.city) is False:
        return BadRequest(f'{new_location.city} is not available as a city in our app. Please try with real location.')

    employer_response = create_employer_response(employer)
    contacts = Contacts(id=employer.contacts_id, phone = employer_response.phone, e_mail = employer_response.e_mail,
                           twitter = employer_response.twitter, address =employer_response.address)

    update_employer = employer_service.update_employer(contacts, new_contacts, new_location.city, employer, new_employer)
    employer_response = create_employer_response(update_employer)

    return employer_response or BadRequest(f'No such no such user')


@employers_router.patch('/info/password')
def update_password(password_data: NewPassword, x_token: str = Header()):
    employer = get_user_or_raise_401(x_token, 'employers')

    employer.password = employer_service.update_employer_password(password_data.current_pass, password_data.new_pass, employer)
    if employer.password:
        return Success(f'password is changed')
    return BadRequest(f'wrong input')

@employers_router.get('/jads')
def get_employer_jads(jad_status: str | None = None, x_token: str = Header()):

    employer = get_user_or_raise_401(x_token, 'employers')
    ads = employer_service.get_employer_ads(employer.id, jad_status)
    return ads


@employers_router.get('/', response_model=list[Employer_Response])
def get_employers(search_by_name: str | None = None,search_by_location: str | None = None , x_token: str = Header()):
    professionals = get_user_or_raise_401(x_token, 'professionals')
    employers_list = employer_service.all(search_by_name, search_by_location)
    return employers_list


@employers_router.post('/matchrequest/{company_ad_id}')
def send_match_request_to_company_ad(company_ad_id: int, x_token: str = Header()):
    employer = get_user_or_raise_401(x_token, "employers")
    company_ad = company_ad_service.get_by_id(company_ad_id)
    if company_ad == None:
        return BadRequest(f'Company ad with id:{company_ad_id} does not exists')
    if company_ad.status_id != 1:
        return BadRequest(f'Company ad with id:{company_ad_id} is not active')
    matched_company_ad = employer_service.send_match_request_to_company_ad(company_ad, employer)
    if matched_company_ad is None:
        return BadRequest(f'Match already requested for "Company ad" with id:{company_ad_id}')

    professional = get_professional_by_id(company_ad.professionals_id)
    return send_mail(f"{professional.first_name} {professional.last_name}", employer.employer_name, company_ad_id)

@employers_router.post('/match/')
def match_requests_in_job_ad(match: Match, x_token: str = Header()):
    employer = get_user_or_raise_401(x_token, "employers")

    employer_is_owner = employer_service.check_if_owner(employer, match.ad_id)
    if employer_is_owner is False:
        return Forbidden(f'Only job ad owner can match a request from job ad id:{match.ad_id}!')

    ad_is_valid = employer_service.check_if_ad_is_valid(match.ad_id)
    if ad_is_valid is False:
        return BadRequest(f"Active job ad with id:{match.ad_id} does not exists")


    for id in match.user_ids:
        job_match_exists = employer_service.job_match_exists(match.ad_id, id)
        if job_match_exists is False:
            return BadRequest(f"Match between id:{match.ad_id}  and {id} does not exists")

        professional_id_are_valid = employer_service.check_if_professional_is_valid(id)
        if professional_id_are_valid is False:
            return BadRequest(f"Active professional with id:{id} does not exists")

    if employer_service.match_requests_in_job_ad(employer, match) is None:
        return BadRequest

    professionals_names = ", ".join(get_professionals_names(match.user_ids))

    return Success(f'"{employer.employer_name}" matched with professionals: {professionals_names}.')
