import unittest
from unittest.mock import Mock, create_autospec, patch
from data.models import Job_ad, New_Job_ad, Contacts, Location, Company_ad, Job_adResponse, Employer, \
    QualificationLevel, Professional, QualificationLevels, Company_Response
from services import company_ad_service

fake_qualification_level = QualificationLevel(
            qualifications_id=1,
            levels_of_qualification_id=1)

fake_qualification_level_str = QualificationLevels(
    qualification = "java",
    level = "beginner")

fake_company_ad = Company_ad(
    id = 1,
    minimum_salary= 1000,
    maximum_salary=1500,
    description="test",
    is_full_remote=False,
    is_partial_remote=False,
    is_main_ad=False,
    status_id=1,
    professionals_id=1,
    location_id=1,
    employers_ids = None,
    qualifications_ids= None)

fake_company_response = Company_Response(
    minimum_salary= 1000,
    maximum_salary=1500,
    description="test",
    is_full_remote=False,
    is_partial_remote=False,
    is_main_ad=False,
    status_id=1,
    professionals_id=1,
    location_id=1,
    qualification_levels= [fake_qualification_level_str])

fake_professional = Professional(id=1,
                username="PeshoTest",
                password="Pesho123",
                first_name="Pesho",
                last_name="Georgiev",
                summary="Turbo Tiger",
                is_busy=False,
                photo=None,
                location=1,
                contacts=1)


class Company_Ad_service_test_should(unittest.TestCase):

    def test_create_Return_CompanyResponseObject(self):
        # arrange &
        company_ad_service.insert_query = Mock()
        company_ad_service.insert_query.return_value = 5
        company_ad_service.add_qualification_levels = Mock()
        # act:
        result = company_ad_service.create(fake_company_response, fake_professional)
        # assert
        self.assertEqual(Company_Response, type(result))
        self.assertEqual(5, result.id)


    def test_create_response_object(self):
        # arrange &
        object = {'id': fake_company_ad.id,
        'minimum_salary':fake_company_ad.minimum_salary,
        'maximum_salary':fake_company_ad.maximum_salary,
        'description': fake_company_ad.description,
        'is_full_remote': fake_company_ad.is_full_remote,
        'is_partial_remote': fake_company_ad.is_partial_remote,
        'is_main_ad': fake_company_ad.is_main_ad,
        'status_id': fake_company_ad.status_id,
        'professionals_id': fake_company_ad.professionals_id,
        'location_id': fake_company_ad.location_id,
        'qualifications_ids': [fake_qualification_level]}
         # act:
        result = company_ad_service.create_response_object(fake_company_ad, fake_professional, [fake_qualification_level])
        # assert
        self.assertEqual(object, result)
        self.assertEqual(object["minimum_salary"], 1000)
        self.assertEqual(object["is_main_ad"], False)
        self.assertEqual(object["professionals_id"], 1)

    def test_update_ReturnsCompanyAd_update_object(self):
        # arrange
        fake_company_ad_new = fake_company_ad
        fake_company_ad_new.minimum_salary = 1100
        fake_company_ad_new.maximum_salary = 2200
         # act:
        result = company_ad_service.update(fake_company_ad, fake_company_ad_new)
        result.professionals_id = fake_company_ad.professionals_id
        # assert
        self.assertEqual(fake_company_ad_new, result)
        self.assertEqual(result.minimum_salary, 1100)
        self.assertEqual(result.maximum_salary, 2200)

    def test_get_match_requestReturnEmployers(self):
        # arrange
        company_ad_service.read_query = Mock()
        company_ad_service.read_query.return_value = [("Coke", "Pepsi")]
         # act:
        result = company_ad_service.get_match_requests(1)
        # assert
        self.assertEqual(["Coke"], result)

    def test_get_location_id_ReturnsID(self):
        # arrange
        location = "Varna"
        company_ad_service.read_query = Mock()
        company_ad_service.read_query.return_value = [(20,)]
         # act:
        result = company_ad_service.get_location_id(location)
        # assert
        self.assertEqual(20, result)

    def test_get_location_id_ReturnsNone_with_invalidLocation(self):
        # arrange
        location = "Varna"
        company_ad_service.read_query = Mock()
        company_ad_service.read_query.return_value = []
         # act:
        result = company_ad_service.get_location_id(location)
        # assert
        self.assertEqual(None, result)

if __name__ == '__main__':
    unittest.main()