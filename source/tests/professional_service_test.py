import unittest
from sqlite3 import IntegrityError
from unittest.case import _AssertRaisesContext
from unittest.mock import Mock, patch

import jwt

from common.auth import create_token
from common.config import JWT_SECRET
from common.responses import BadRequest
from data.models import (Company_adResponse, Contacts, Location, LoginData, Professional,
                         ProfessionalResponseModel)
from services import professional_service

mock_professional_service = Mock(spec='services.professional_service')

fake_professional = Professional(id=1,
                username="PeshoTest",
                password="Pesho123",
                first_name="Pesho",
                last_name="Georgiev",
                summary="Turbo Tiger",
                is_busy=False,
                photo=None,
                location=1,
                contacts=1)

fake_professional_2 = Professional(id=2,
                username="GoshoTest",
                password="Gosho123",
                first_name="Gosho",
                last_name="Petrov",
                summary="Mega Tiger",
                is_busy=True,
                photo=None,
                location=1,
                contacts=1)

fake_contacts = Contacts(id=1,
                         phone="0899123123",
                         e_mail = "pesho@telerik.com",
                         twitter = "Pepkata",
                         address = "Pernik str 123")

fake_location = Location(id=1, city="Sofia")

class ProfessionalServiceShould(unittest.TestCase):
    def test_create_professional_with_valid_data(self):
        #arrange:((
        professional_service.get_location_id = Mock()
        professional_service.get_location_id.return_value = '1'
        contacts_id = 3
        insert_data_func = lambda arg1, arg2: contacts_id

        #act
        result = professional_service.create(fake_professional, fake_location, fake_contacts, insert_data_func)

        #assert
        self.assertEqual(Professional, type(result))
        self.assertEqual("PeshoTest", result.username)
        self.assertEqual("Pesho", result.first_name)
        self.assertEqual("Georgiev", result.last_name)
        self.assertEqual("Turbo Tiger", result.summary)
        self.assertEqual(False, result.is_busy)
        self.assertEqual(3, result.contacts)
        self.assertEqual('1', result.location)

   
    def test_login_returns_Professional_when_valid_loginData(self):
        professional_service.find_by_username = Mock()
        professional_service.find_by_username.return_value = fake_professional
        professional_service._hash_password = Mock()
        professional_service._hash_password.return_value = fake_professional.password

        #act
        result = professional_service.login(fake_professional.username, fake_professional.password)

        # assert
        self.assertEqual(Professional, type(result))
        self.assertEqual("PeshoTest", result.username)
        self.assertEqual("Pesho123", result.password)

    def test_login_returns_None_when_loginData_invalid(self):
        #arrange:
        professional_service.find_by_username = Mock()
        professional_service.find_by_username.return_value = fake_professional
        professional_service._hash_password = Mock()
        professional_service._hash_password.return_value = "xxx"

        #act
        result = professional_service.login(fake_professional.username, fake_professional.password)

        # assert
        self.assertEqual(None, result)

    def test_get_contacts_returns_professional_Contacts(self):
        
        with patch('services.professional_service.read_query') as mock_data:
            mock_data.return_value = [(1, "0899123123","pesho@telerik.com", \
                         "Pepkata", "Pernik str 123")]
            expected = Contacts(id=1,
                         phone="0899123123",
                         e_mail = "pesho@telerik.com",
                         twitter = "Pepkata",
                         address = "Pernik str 123")
            result = professional_service.get_contacts(1, get_data_func=mock_data)
        self.assertEqual(expected, result)

    def  test_create_response_BadRequest_when_city_invalid(self):
        #arrange
        with patch('services.professional_service.location_exists') as mock_data:
            mock_data.return_value = False
        #act
            result = professional_service.create(fake_professional, fake_location, fake_contacts, insert_data_func=mock_data)

        # assert
        self.assertIsInstance(result, BadRequest)

    def test_get_professional_by_id_returnProfessional(self):
        #arrange:
        insert_data_func = lambda arg1, arg2: [(1, "PeshoTest", "Pesho123",
                                "Pesho","Georgiev","Turbo Tiger", False, None, 1,1)]
        #act
        result = professional_service.get_professional_by_id(1, insert_data_func)
        # assert
        self.assertEqual(Professional, type(result))

    def test_get_professional_by_id_returnNone_when_Professional_Id_Invalid(self):
        #arrange:
        insert_data_func = lambda arg1, arg2: None
        #act
        result = professional_service.get_professional_by_id(1, insert_data_func)
        # assert
        self.assertEqual(None, result)

    def test_update_password_returns_true_when_validData(self):
        # arrange
        fake_pass = "Ivan123"
        fake_new_pass = "Ivan111"
        professional_service._hash_password = Mock()
        professional_service._hash_password.return_value = "1111"
        professional_service.update_query = Mock()
        professional_service.update_query.return_value = [(1,)]
        professional = fake_professional
        professional.password = fake_pass
         # act:
        expected = [(1,)] 
        result = professional_service.update_password(fake_pass, fake_new_pass, professional)
        # assert
        self.assertEqual(expected, result)

    def test_update_passwordReturns_None_with_wrong_input(self):
        # arrange
        fake_pass = "Ivan123"
        fake_new_pass = "Ivan111"
        professional_service.update_query = Mock()
        professional_service.update_query.return_value = []
        professional = fake_professional
        professional.password = professional_service._hash_password("wrong_pass")
         # act:
        result = professional_service.update_password(fake_pass, fake_new_pass, professional)
        # assert
        self.assertFalse(result)

    def test_update_info_updates_professional_info_when_valid_data(self):
        with patch('services.professional_service.update_query') as mock_update:
            professional = fake_professional
            professional_service.get_location_by_id = Mock()
            professional_service.get_location_by_id.return_value = 'Sofia'
            professional_service.get_contacts = Mock()
            professional_service.get_contacts.return_value = fake_contacts
            new_professional = Professional(first_name="Georgi", last_name="Georgiev", summary="Testing Tiger")
            result = professional_service.update_info(new_professional, professional, location = None, new_contacts=None)
            self.assertEqual(new_professional.first_name, result.first_name)
            self.assertEqual(new_professional.last_name, result.last_name)
            self.assertEqual(new_professional.summary, result.summary)


    def test_professional_update_info_updates_location_with_validData(self):
        #arrange
         with patch('services.professional_service.update_query') as mock_update:
            professional = fake_professional
            new_location = Location(city = "Varna")
            professional_service.get_location_id_by_city = Mock()
            professional_service.get_location_id_by_city.return_value = 2
            professional_service.location_exists = Mock()
            professional_service.location_exists.return_value = True
            professional_service.get_location_by_id = Mock()
            professional_service.get_location_by_id.return_value = 'Varna'
            professional_service.get_contacts = Mock()
            professional_service.get_contacts.return_value = fake_contacts
            #act
            result = professional_service.update_info(professional = None, current_professional =professional, location = new_location, new_contacts=None)
            #assert
            self.assertEqual(new_location.city, result.location)

    def test_professional_update_info_returns_BadReqyest_with_invalidData(self):
        #arrange
        professional = fake_professional
        new_location = Location(city = "Varna")
        professional_service.location_exists = Mock()
        professional_service.location_exists.return_value = False
        #act
        result = professional_service.update_info(professional = None, current_professional =professional, location = new_location, new_contacts=None)
        #assert
        self.assertIsInstance(result, BadRequest)

    def test_get_ads_returns_EmptyList_when_NoAds_with_givenStatus(self):
        # arrange:
        professional_service.read_query = Mock()
        professional_service.read_query.return_value = []
        # act
        result = professional_service.get_ads(1, None)
        # assert
        self.assertEqual([],list(result))

    def test_get_ads_return_correct_numberOfAds_when_valid_status(self):
        # arrange:
        professional_service.read_query = Mock()
        professional_service.read_query.return_value = (1,)
        professional_service.read_query = Mock()
        professional_service.read_query.return_value = [
            (1, 1200, 1500, "test tiger", 0, 0, 0, 3, 1, 5), (3, 1100, 1300, "test_description2", 0, 0, 1, 1, 1, 4)]
        professional_service.get_employers = Mock()
        professional_service.get_employers.return_value = ['Cola Test']
        professional_service.get_qualifications = Mock()
        professional_service.get_qualifications.return_value = []
        # act
        result = list(professional_service.get_ads(1, "active"))
        # assert
        self.assertEqual(2, len(result))

    def test_get_ads_return_correct_numberOfAds_when_no_status(self):
        # arrange:
        professional_service.read_query = Mock()
        professional_service.read_query.return_value = []
        professional_service.read_query = Mock()
        professional_service.read_query.return_value = [(1, 1200, 1500, "test tiger", 0, 0, 0, 3, 1, 5)]
        professional_service.get_employers = Mock()
        professional_service.get_employers.return_value = ['Cola Test', 'Derbi Cola Test']
        professional_service.get_qualifications = Mock()
        professional_service.get_qualifications.return_value = []
        # act
        result = list(professional_service.get_ads(1, None))
        # assert
        self.assertEqual(1, len(result))

    


if __name__ == '__main__':
    unittest.main()