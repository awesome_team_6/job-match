import unittest
from unittest.mock import Mock, create_autospec, patch
from data.models import Job_ad, New_Job_ad, Contacts, Location, Company_ad, Job_adResponse, Employer, QualificationLevel
from services import job_ad_service

fake_qualification_level = QualificationLevel(
            qualifications_id=1,
            levels_of_qualification_id=2)

fake_job_ad = Job_ad(
    id = 1,
    minimum_salary= 1000,
    maximum_salary=1500,
    description="aaaa",
    is_full_remote=False,
    is_partial_remote=False,
    location_id=1,
    status_id=1,
    employer_id=1,
    professionals_ids = [1],
    qualifications_ids= [fake_qualification_level])


fake_employer = Employer(id=1,
                         username="Spiridon",
                         password = "2222",
                         employer_name = "Spirola",
                         description = "Big company",
                         logo = "link",
                         location = 1,
                         contacts = None,
                         active_job_ads = 0,
                         successful_matches =1)


class Job_Ad_service_test_should(unittest.TestCase):

    # def test_get_all_ads_ReturnList_ofJobAds(self):
    #     # arrange:
    #     job_ad_service.read_query= Mock()
    #     job_ad_service.read_query.return_value = [
    #         (2, 1100, 1200, "test_description2", 0, 0, 3, 1, 5), (3, 1100, 1300, "test_description2", 0, 0, 2, 1, 4)]
    #     job_ad_service.get_qualifications = Mock()
    #     job_ad_service.get_qualifications.return_value = [1,2]
    #
    #     # act
    #     result = list(job_ad_service.get_all("Sofia",100, 200,1,"Java",1,None,None))
    #     # assert
    #     self.assertEqual(2, len(result))

    def test_create_response_object(self):
        # arrange &
        object = {'id': fake_job_ad.id,
        'minimum_salary':fake_job_ad.minimum_salary,
        'maximum_salary':fake_job_ad.maximum_salary,
        'employers_id': fake_employer.id,
        'description': fake_job_ad.description,
        'qualifications_ids': [fake_qualification_level]}
         # act:
        result = job_ad_service.create_response_object(fake_job_ad, fake_employer, [fake_qualification_level])
        # assert
        self.assertEqual(object, result)
        self.assertEqual(object["minimum_salary"], 1000)
        self.assertEqual(object["employers_id"], 1)


    def test_get_by_id_returnJob_adResponse(self):
        #arrange:
        job_ad_service.read_query = Mock()
        job_ad_service.read_query.return_value = [(2, 1100, 1200, "test_description2", 0, 0, 3, 1, 5), (3, 1100, 1300, "test_description2", 0, 0, 2, 1, 4)]
        job_ad_service.get_qualifications = Mock()
        job_ad_service.get_qualifications.return_value = [1,2]
        #act
        result = job_ad_service.get_by_id(1)
        # assert
        self.assertEqual(Job_adResponse, type(result))
        self.assertEqual(2, result.id)

    def test_get_by_id_returnNone_invalidID(self):
        #arrange:
        job_ad_service.read_query = Mock()
        job_ad_service.read_query.return_value = []
        job_ad_service.get_qualifications = Mock()
        job_ad_service.get_qualifications.return_value = [1,2]
        #act
        result = job_ad_service.get_by_id(1)
        # assert
        self.assertEqual(None, result)



    def test_get_job_ads_status_ReturnStatus(self):
        # arrange:
        job_ad_service.read_query_single = Mock()
        job_ad_service.read_query_single.return_value = [("active")]
        # act
        result = job_ad_service.get_job_ad_status(1)
        # assert
        self.assertEqual("active", result)

if __name__ == '__main__':
    unittest.main()
