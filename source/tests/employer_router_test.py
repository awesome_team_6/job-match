import unittest
from unittest.mock import Mock

from data.models import LoginData, Employer, Location, Contacts, Employer_Response, Company_ad, NewPassword

mock_employer_service = Mock(spec='services.employer_service')

fake_employer = Employer(id=2,
                         username="Spiridon",
                         password = "2222",
                         employer_name = "Spirola",
                         description = "Big company",
                         location = 1,
                         contacts = None,
                         logo =None)


fake_pass = NewPassword(current_pass = "old_pass",
    new_pass = "new_pass")

fake_contact = Contacts(id=1,
                         phone="08889990",
                         e_mail = "fake@yahoo.com",
                         twitter = "Spirola",
                         address = "mladost 1")

fake_location = Location(id=1,
                         city="Pernik")

fake_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiU3Bpcmlkb24ifQ.--07QjBN0lLP_LzZR8s_BLDrxedq0ht33E3JgkHWokE"

fake_company_ad = Company_ad(
    id = 1,
    minimum_salary= 1000,
    maximum_salary=1500,
    description="aaaa",
    is_full_remote=False,
    is_partial_remote=False,
    is_main_ad=False,
    status_id=1,
    professionals_id=1,
    location_id=1,
    employers_ids = None,
    qualifications_ids= None)

from common.responses import BadRequest
from routers import employers as employers_router

class EmployerRouter_Should(unittest.TestCase):
    def test_postEmployerRegisterReturns_BadRequest_whenInvalid_location(self):
        employers_router.validators.location_exists = Mock()
        employers_router.validators.location_exists.return_value = False
        result = employers_router.register(fake_employer, fake_location, fake_contact)
        print(fake_employer)
        self.assertEqual(400, result.status_code)

    def test_postEmployerRegisterReturns_BadRequest_whenEmployer_is_None(self):
        employers_router.employer_service.create = Mock()
        employers_router.employer_service.create.return_value = None
        result = employers_router.register(fake_employer, fake_location, fake_contact)
        self.assertEqual(400, result.status_code)


    def test_postEmployerRegisterReturns_EmployerRespond_when_input_is_valid(self):
        employers_router.employer_service.create = Mock()
        employers_router.employer_service.create.return_value = fake_employer
        result = employers_router.register(fake_employer, Location(id=1, city="Sofia"), fake_contact)
        print(result)
        self.assertEqual(Employer_Response, type(result))

    def test_postEmployerLoginReturns_BadRequest_whenInvalid_input(self):
        employers_router.employer_service.try_login = Mock()
        employers_router.employer_service.try_login.return_value = None
        result = employers_router.login(LoginData(username=fake_employer.username,password =fake_employer.password))

        self.assertEqual(400, result.status_code)
        self.assertEqual(b'Invalid login data', result.body)

    def test_postEmployerLoginReturns_Token_when_input_isValid(self):

        employers_router.employer_service.try_login = Mock()
        employers_router.employer_service.try_login.return_value = fake_employer
        result = employers_router.login(LoginData(username=fake_employer.username,password =fake_employer.password ))

        self.assertEqual({'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiU3Bpcmlkb24ifQ.--07QjBN0lLP_LzZR8s_BLDrxedq0ht33E3JgkHWokE'}, result)

    def test_getEmployerInfoReturns_EmployerResponse_when_token_is_valid(self):
        employers_router.auth.get_user_or_raise_401 = Mock()
        employers_router.auth.get_user_or_raise_401.return_value = fake_employer
        result = employers_router.employer_service.create_employer_response(fake_employer)
        self.assertEqual(Employer_Response, type(result))

    def test_match_professional_ad_ReturnBadRequest_if_already_matched(self):
        employers_router.auth.get_user_or_raise_401 = Mock()
        employers_router.auth.get_user_or_raise_401.return_value = fake_employer
        result = employers_router.employer_service.send_match_request_to_company_ad(fake_company_ad, fake_employer)
        print(result)

        self.assertEqual(None, result)

    # def test_match_update_pass_ReturnBadRequest_if_invalidInput(self):
    #       employers_router.auth.get_user_or_raise_401 = Mock()
    #       employers_router.auth.get_user_or_raise_401.return_value = fake_employer
    #       fake_employer.password = Mock()
    #       fake_employer.password.return_value = None
    #       result = employers_router.update_password(fake_pass,"str")
    #       self.assertEqual(BadRequest, result)


if __name__ == '__main__':
    unittest.main()
