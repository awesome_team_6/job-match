import unittest
from unittest.mock import Mock
import jwt
from common.auth import create_token
from common.config import JWT_SECRET
from common.validators import location_exists

from data.models import LoginData, Professional, Location, Contacts

mock_professional_service = Mock(spec='services.professional_service')

fake_professional = Professional(id=1,
                         username="PeshoTest",
                         password="123",
                         first_name="Pesho",
                         last_name="Peshov",
                         summary="Turbo Tiger",
                         is_busy=False,
                         foto=None)


fake_contacts = Contacts(id=1,
                         phone="0899123123",
                         e_mail = "pesho@telerik.com",
                         twitter = "Pepkata",
                         address = "Pernik str 123")

fake_location = Location(id=1,
                         city="Pernik")

from common.responses import BadRequest
from routers import professionals as professionals_router

class ProfessionalRouter_Should(unittest.TestCase):
    def test_postProfessionalRegisterReturns_BadRequest_whenInvalid_city(self):
        professionals_router.professional_service.location_exists = Mock()
        professionals_router.professional_service.location_exists.return_value = False
        result = professionals_router.register(fake_professional, fake_location, fake_contacts)
        self.assertEqual(400, result.status_code)

    def test_postProfessionalLoginReturns_BadRequest_whenInvalid_loginData(self):

        professionals_router.professional_service.login = Mock()
        professionals_router.professional_service.login.return_value = None
        result = professionals_router.login(LoginData(username=fake_professional.username,password=fake_professional.password))

        self.assertEqual(400, result.status_code)
        self.assertEqual(b'Invalid login data', result.body)

    def test_postProfessionalLoginReturns_Token_whenValid_loginData(self):

        professionals_router.professional_service.login = Mock()
        professionals_router.professional_service.login.return_value = fake_professional
        result = professionals_router.login(LoginData(username=fake_professional.username, password=fake_professional.password ))
        fake_token =  jwt.encode({"user": fake_professional.username}, JWT_SECRET, algorithm="HS256")
        expected = {"token": fake_token}
        self.assertEqual(expected, result)

    #pesho_jwt = eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlBlc2hvIn0.3SuKWVvSOvNBMKqzNVEzOnSWEiQc-fXyr_LbK03gNjM


if __name__ == '__main__':
    unittest.main()
