import unittest
from unittest.mock import Mock, create_autospec, patch

from common.auth import _hash_password
from data.models import Employer, Employer_Response, Contacts, Location, Company_ad, Job_adResponse
from services import employer_service
from mariadb import IntegrityError

fake_employer = Employer(id=1,
                         username="Spiridon",
                         password = "2222",
                         employer_name = "Spirola",
                         description = "Big company",
                         logo = "link",
                         location = 1,
                         contacts = None,
                         active_job_ads = 0,
                         successful_matches =1)

fake_company_ad = Company_ad(
    id = 1,
    minimum_salary= 1000,
    maximum_salary=1500,
    description="aaaa",
    is_full_remote=False,
    is_partial_remote=False,
    is_main_ad=False,
    status_id=1,
    professionals_id=1,
    location_id=1,
    employers_ids = None,
    qualifications_ids= None)



class EmployerServices_Should(unittest.TestCase):
    def test_create_employer_when_data_isValid(self):

        #arrange
        loaction_id= "2"
        check_location = lambda arg1: True
        read_single_data_func = lambda arg1, arg2: loaction_id
        generated_contact_id = 2
        insert_data_func = lambda arg1, arg2: generated_contact_id
        upload_logo = lambda arg1, arg2: "str"

        #act
        result = employer_service.create("testov", "1234", "cola", "em of the year", "logo",
                                         "088", "hr@cola.com", "twit", "test adress", "Plovdiv", insert_data_func, read_single_data_func, check_location, upload_logo )
        #assert
        self.assertEqual(Employer, type(result))


    def test_create_employer_when_location_NotValid(self):

        #arrange
        loaction_id= "2"
        read_single_data_func = lambda arg1, arg2: loaction_id
        generated_contact_id = 2
        insert_data_func = lambda arg1, arg2: generated_contact_id
        check_location = lambda arg1: False
        upload_logo = lambda arg1, arg2: "str"

        #act
        result = employer_service.create("testov", "1234", "cola", "em of the year", "logo",
                                         "088", "hr@cola.com", "twit", "test adress", "Plovdiv", insert_data_func, read_single_data_func, check_location, upload_logo )
        #assert
        self.assertEqual("No location", result)

    def test_create_employer_response_when_data_isValid(self):
        #arrange:
        get_data_func = lambda arg1, arg2:[(2, "test_name", "cola",
                                "test descr", "test_link", "Sofia", "08888888", "test@yahoo.com", None, "address test", 0,0)]


        #act
        result = employer_service.create_employer_response(fake_employer, get_data_func)

        # assert
        self.assertEqual(Employer_Response, type(result))
    def test_create_employer_response_ReturnNone_when_data_isNotValid(self):
        #arrange:
        get_data_func = lambda arg1, arg2:[]
        #act
        result = employer_service.create_employer_response(fake_employer, get_data_func)

        # assert
        self.assertEqual(None, result)

    def test_try_login_ReturnEmployer_when_data_isValid(self):
        #arrange:
        employer_service.find_by_username = Mock()
        employer_service.find_by_username.return_value = fake_employer
        employer_service._hash_password = Mock()
        employer_service._hash_password.return_value = fake_employer.password

        #act
        result = employer_service.try_login(fake_employer.username, fake_employer.password)

        # assert
        self.assertEqual(Employer, type(result))

    def test_try_login_ReturnNone_when_password_is_NotValid(self):
        #arrange:
        employer_service.find_by_username = Mock()
        employer_service.find_by_username.return_value = fake_employer
        employer_service._hash_password = Mock()
        employer_service._hash_password.return_value = "wrong pass"

        #act
        result = employer_service.try_login(fake_employer.username, fake_employer.password)


        # assert
        self.assertEqual(None, result)

    def test_try_login_ReturnNone_when_no_User(self):
        #arrange:
        employer_service.find_by_username = Mock()
        employer_service.find_by_username.return_value = None

        #act
        result = employer_service.try_login(fake_employer.username, fake_employer.password)
        print(result)

        # assert
        self.assertEqual(None, result)

    def test_match_company_ad_when_data_isValid(self):
        #arrange:
        insert_data_func = lambda arg1, arg2: tuple()
        #act
        result = employer_service.send_match_request_to_company_ad(fake_company_ad,fake_employer, insert_data_func)
        # assert
        self.assertEqual(fake_employer, result)

    def test_get_employer_by_id_returnEmployer(self):
        #arrange:
        insert_data_func = lambda arg1, arg2: [(1, "Spiridon", "2222",
                                "Spirola", "Big company", "link", None,None)]
        #act
        result = employer_service.get_employer_by_id(1, insert_data_func)
        # assert
        self.assertEqual(fake_employer, result)

    def test_get_employer_by_id_returnNone_when_noEmployerwithid(self):
        #arrange:
        insert_data_func = lambda arg1, arg2: None
        #act
        result = employer_service.get_employer_by_id(1, insert_data_func)
        # assert
        self.assertEqual(None, result)

    def test_all_createsListOfEmployers_when_dataIsPresent(self):
        # arrange:
        get_data_func = lambda arg1: \
            [(2, "test_name", "pass_test1", "test descr1", "test_link1", "Sofia", "08888888", "test@yahoo.com", None, "address test", 0,0),
             (3, "test_name", "pass_test2", "test descr2", "test_link2", "Sofia", "08888887", "test2@yahoo.com", None, "address test2",1, 1)]
        # act
        result = list(employer_service.all(search_by_name = None, search_by_location = None, get_data_func=get_data_func))
        # assert
        self.assertEqual(2, len(result))

    def test_get_employer_ads_ReturnEmptyList_when_NoAds_with_givenStatus(self):
        # arrange:
        employer_service.read_query_single = Mock()
        employer_service.read_query_single.return_value = None
        # act
        result = employer_service.get_employer_ads(1, "active")
        # assert
        self.assertEqual([], result)

    def test_get_employer_ads_ReturnList_ofJobAds(self):
        # arrange:
        employer_service.read_query_single = Mock()
        employer_service.read_query_single.return_value = (1,)
        employer_service.read_query = Mock()
        employer_service.read_query.return_value = [
            (2, 1100, 1200, "test_description2", 0, 0, 3, 1, 5), (3, 1100, 1300, "test_description2", 0, 0, 2, 1, 4)]
        # act
        result = list(employer_service.get_employer_ads(1, "active"))
        # assert
        self.assertEqual(2, len(result))

    def test_job_match_exists_ReturnTrue(self):
        # arrange:
        employer_service.read_query_single = Mock()
        employer_service.read_query_single.return_value = (1,)
        # act
        result = employer_service.job_match_exists(1, 1)
        # assert
        self.assertEqual(True, result)

    def test_job_match_exists_ReturnFalse(self):
        # arrange:
        employer_service.read_query_single = Mock()
        employer_service.read_query_single.return_value = None
        # act
        result = employer_service.job_match_exists(1, 1)
        # assert
        self.assertEqual(False, result)

    def test_update_passwordReturns_Password(self):
        # arrange
        fake_pass = "1111"
        fake_new_pass = "1111"
        employer_service._hash_password = Mock()
        employer_service._hash_password.return_value = "1111"
        employer_service.update_query = Mock()
        employer_service.update_query.return_value = None
        employer = fake_employer
        employer.password = fake_pass
         # act:
        result = employer_service.update_employer_password(fake_pass, fake_new_pass, employer)
        # assert
        self.assertEqual("1111", result)
    def test_update_passwordReturns_None_with_wrong_input(self):
        # arrange
        fake_pass = "1111"
        fake_new_pass = "2222"
        employer_service.update_query = Mock()
        employer_service.update_query.return_value = None
        employer = fake_employer
        employer.password = _hash_password("wrong_pass")
         # act:
        result = employer_service.update_employer_password(fake_pass, fake_new_pass, employer)
        # assert
        self.assertEqual(None, result)


