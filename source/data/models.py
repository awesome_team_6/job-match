from datetime import date, datetime
from email.policy import default
from pydantic import BaseModel,EmailStr, constr

class Contacts(BaseModel):
    id: int | None
    phone: str | None
    e_mail: EmailStr | None
    twitter: str | None
    address: str | None

class Location(BaseModel):
    id: int | None
    city: str | None

TUsername = constr(regex='^\w{2,100}$')
TPassword = constr(regex='^\w{3,100}$')

class Employer_Response(BaseModel):
    id: int | None
    username: str | None
    employer_name: str | None
    description: str | None
    logo: str | None
    location: str | None
    phone : str | None
    e_mail: str | None
    twitter: str | None
    address: str | None
    active_job_ads: int | None
    successful_matches: int | None

    @classmethod
    def from_query_result(cls, id, username, employer_name, description,logo, location,
                          phone, e_mail, twitter, address, active_job_ads, successful_matches):
        return cls(
            id=id,
            username=username,
            employer_name=employer_name,
            description=description,
            logo = logo,
            location=location,
            phone=phone,
            e_mail=e_mail,
            twitter=twitter,
            address=address,
            active_job_ads = active_job_ads,
            successful_matches = successful_matches
        )

class Employer(BaseModel):
    id: int | None
    username: str | None
    password: str | None
    employer_name: str | None
    description: str | None
    logo: str | None
    location_id: int | None
    contacts_id: int | None

    @classmethod
    def from_query_result(cls, id, username,password, employer_name, description, logo, location_id, contacts_id):
        return cls(
            id=id,
            password =password,
            username=username,
            employer_name=employer_name,
            description=description,
            logo = logo,
            location_id = location_id,
            contacts_id = contacts_id)

class Professional(BaseModel):
    id: int | None
    username: TUsername | None
    password: TPassword | None
    first_name: str | None
    last_name: str | None
    summary: str | None
    is_busy: bool | None
    photo: str | None
    location: str | int | None
    contacts: int | None

    @classmethod
    def from_query_result(cls, id, username, password, first_name, last_name, summary, is_busy, photo, location, contacts):
        return cls(
            id=id,
            username=username,
            password=password,
            first_name=first_name,
            last_name=last_name,
            summary=summary,
            is_busy=is_busy,
            photo=photo,
            location=location,
            contacts=contacts)

class QualificationLevel(BaseModel):
    qualifications_id: int
    levels_of_qualification_id:int

    @classmethod
    def from_query_result(cls, qualifications_id, levels_of_qualification_id):
        return cls(
            qualifications_id=qualifications_id,
            levels_of_qualification_id=levels_of_qualification_id)

class QualificationLevels(BaseModel):
    qualification: str
    level:str

    @classmethod
    def from_query_result(cls, qualification, level):
        return cls(
            qualification=qualification,
            level=level)

class Job_ad(BaseModel):
    id: int | None
    minimum_salary: int | None
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool | None 
    is_partial_remote: bool | None 
    location_id: int | None
    status_id: int | None
    employers_id: int | None
    professionals_ids: list[int|None] | None = None
    qualifications_ids: list[QualificationLevel] | None = None

    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary, description, employers_id, qualifications_ids = []):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            employers_id = employers_id,
            description=description,
            qualifications_ids=qualifications_ids)

class Company_ad(BaseModel):
    id: int | None
    minimum_salary: int | None 
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool | None
    is_partial_remote: bool | None 
    is_main_ad: bool | None
    status_id: str |int | None
    professionals_id: int | None
    location_id: str|int | None
    qualifications_ids: list[QualificationLevel] | None = None

    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary,is_full_remote,is_partial_remote,is_main_ad,status,professionals_id,location, description): # , qualifications_ids = []):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            description=description,
            is_full_remote=is_full_remote,
            is_partial_remote=is_partial_remote,
            is_main_ad=is_main_ad,
            status=status,
            professionals_id=professionals_id,
            location=location)

class LoginData(BaseModel):
    username: TUsername
    password: TPassword

class ProfessionalResponseModel(BaseModel):
    username: str
    first_name: str
    last_name: str
    summary: str
    is_busy: str
    photo: str | None
    location: str 
    contacts: Contacts

class ContactsResponseModel(BaseModel):
    id:int
    phone: str | None
    e_mail: str
    twitter: str | None
    address: str | None

class NewPassword(BaseModel):
    current_pass: TPassword
    new_pass: TPassword

class Company_ad_Response(BaseModel):
    id: int | None
    minimum_salary: int | None 
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool | None
    is_partial_remote: bool | None 
    is_main_ad: bool | None
    status: str | None
    username: str | None
    location_id: int | None
    #match_requests: list
    qualifications_ids: list

    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status,username,location_id, qualifications_ids):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            description=description,
            is_full_remote=is_full_remote,
            is_partial_remote=is_partial_remote,
            is_main_ad=is_main_ad,
            status=status,
            username=username,
            location_id=location_id,
            #employers_ids = employers_ids,
            qualifications_ids = qualifications_ids)


class Job_adResponse(BaseModel):
    id: int | None
    minimum_salary: int | None
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool | None 
    is_partial_remote: bool | None 
    location_id: int | None
    status_id: int | None
    employers_id: int | None
    professionals_ids: list[int|None] | None = None
    qualifications_ids: list

    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id,qualifications_ids = []):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            description=description,
            is_full_remote=is_full_remote, 
            is_partial_remote=is_partial_remote,
            location_id=location_id,
            status_id=status_id,
            employers_id=employers_id,
            qualifications_ids=qualifications_ids)

class ProfessionalInfo(BaseModel):
    first_name: str
    last_name: str
    summary : str
    photo: str | None
    location: str
    status: str # Active – the only status that allows active Company ads or Busy – all Company ads must be hidden or private
    active_ads: int # Currently active number of Company ads
    matches: list | None # (must) – (should)could be visible or hidden
    
    @classmethod
    def from_query_result(cls, first_name, last_name, summary, location, status, photo, active_ads, matches):
        return cls(
            first_name = first_name,
            last_name = last_name,
            summary=summary,
            location=location,
            status=status,
            photo=photo,
            active_ads=active_ads,
            matches=matches)

class New_Company_Ad(BaseModel):
    id: int | None
    minimum_salary: int | None 
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool |None
    is_partial_remote: bool | None
    is_main_ad: bool | None
    status_id: int | None
    location_id: int | None
    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,location_id):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            description=description,
            is_full_remote=is_full_remote,
            is_partial_remote=is_partial_remote,
            is_main_ad=is_main_ad,
            status_id=status_id,
            location_id=location_id)

class New_Job_ad(BaseModel):
    id: int | None
    minimum_salary: int | None
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool | None 
    is_partial_remote: bool | None 
    location_id: int | None
    status_id: int | None
    

    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            description=description,
            is_full_remote=is_full_remote, 
            is_partial_remote=is_partial_remote,
            location_id=location_id,
            status_id=status_id)

class Match(BaseModel):
    ad_id: int
    user_ids: list[int]

class Company_adResponse(BaseModel):
    id: int | None
    minimum_salary: int | None 
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool | None
    is_partial_remote: bool | None 
    is_main_ad: bool | None
    status_id: int | None
    professionals_id: int | None
    location_id: int | None
    employers_ids: list
    qualifications_ids: list

    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id, employers_ids,qualifications_ids):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            description=description,
            is_full_remote=is_full_remote,
            is_partial_remote=is_partial_remote,
            is_main_ad=is_main_ad,
            status_id=status_id,
            professionals_id=professionals_id,
            location_id=location_id,
            employers_ids = employers_ids,
            qualifications_ids = qualifications_ids)

class Company_Response(BaseModel):
    id: int | None
    minimum_salary: int | None 
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool | None
    is_partial_remote: bool | None 
    is_main_ad: bool | None
    status_id: int| None
    professionals_id: str | None
    location_id: int | None
    #match_requests: list
    qualification_levels: list[QualificationLevels]

    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id, qualification_levels):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            description=description,
            is_full_remote=is_full_remote,
            is_partial_remote=is_partial_remote,
            is_main_ad=is_main_ad,
            status_id=status_id,
            professionals_id=professionals_id,
            location_id=location_id,
            #employers_ids = employers_ids,
            qualification_levels = qualification_levels)

class CompanyResponseModel(BaseModel):
    id: int | None
    minimum_salary: int | None 
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool | None
    is_partial_remote: bool | None 
    is_main_ad: bool | None
    status_id: int | None
    professionals_id: int | None
    location_id: int | None
    qualifications_ids: list

    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,is_main_ad,status_id,professionals_id,location_id, qualifications_ids):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            description=description,
            is_full_remote=is_full_remote,
            is_partial_remote=is_partial_remote,
            is_main_ad=is_main_ad,
            status_id=status_id,
            professionals_id=professionals_id,
            location_id=location_id,
            qualifications_ids = qualifications_ids)

class JobResponseModel(BaseModel):
    id: int | None
    minimum_salary: int | None
    maximum_salary: int | None
    description: str |None
    is_full_remote: bool | None 
    is_partial_remote: bool | None 
    location_id: str|int | None
    status_id: str |int | None
    employers_id: int | None
    #professionals_ids: list
    qualification_levels: list[QualificationLevels]

    @classmethod
    def from_query_result(cls, id, minimum_salary, maximum_salary, description,is_full_remote,is_partial_remote,location_id,status_id,employers_id,qualification_levels):
        return cls(
            id=id,
            minimum_salary=minimum_salary,
            maximum_salary=maximum_salary,
            description=description,
            is_full_remote=is_full_remote, 
            is_partial_remote=is_partial_remote,
            location_id=location_id,
            status_id=status_id,
            employers_id=employers_id,
            #professionals_ids= professionals_ids,
            qualification_levels=qualification_levels)
