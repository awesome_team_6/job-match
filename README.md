# Job Match

RESTFull API for matching professionals and companies that are looking for their best new colleagues. 

## About 

The application has two main parts with very much alike structure:

**Employer Part** – Each company has the ability to create and update an eployer's account. Employers can create job ads containing the required qualifications, level of qualification, salary range, description, location and/or remote etc to present the available positions in the company. Companies can also search through ads(company ads) that are created from professionals looking for new opportunities.

**Professionals Part** – Individuals create and manage a professional account. They can create company ads - to present themselves to potential future employers. The company ads include information about the qualifications, expertise, location ot remote, salary ranges, etc of the owner. Same like employers, professionals can search through employers' job ads. 

Subparts of the above are respectively - the **Job ads and Company ads**, which are again very similar to one another.

## Set up

### Technical Requirements

- Python 3.9 +
- MariaDb
- FastAPI
- Uvicorn

### Database 

We use SQL database. The project was developed on MariaDb using MySQL Workbech. Below you may find the DB Schema and short description of each class.

![Database Schema](./db_scripts/job_match_schema.png)

- *cad_match_requests* - Stores the information for all company ads that have received match requests from an employers.
- *company ads* - All ads created by professionals.
- *contacts* - Both professionals and employers have contacts info, which is kept in the contacts table. A foreign key is saved in the respective account.
- *employers* - All company accounts reside here.
- *jad_match_requests* - Stores the information for all job ads that have received match requests from a professional.
- *job_ads* - Ads creted by employers
- *levels of qualification* - The different levels of qualification. Predifined!
- *locations* - Predefined table with all Bulgarian cities, as the app is developed for the Bulgarian market
- *professional_qualifications_levels* - The qualifications and their levels stated in a company ad. Contains foreign keys for the ad, qualification and its level.
- *professionals* - All professional accounts belong here
- *qualifications* - Preset collection of qualifications. Can be extended when an ad is created with new skill
- *qualifications levels* - The qualifications and their levels stated in a job ad. Contains foreign keys for the ad, qualification and its level.
- *status* - Preset collection of allowed ad status.

### Running the project locally 

- [Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) the repo. 
- Set up your database. You may import the database structure from db_scripts folder - job_match_structure_only.sql file. Project was developed using the Microsoft MySQL Workbench, but you can use a tool of your choice to import and manage the database.
- Fill the database with data. Use the provided job_match_data_only script from db_scripts folder.
- Run the project with ```uvicorn main:app``` command. You should see something like:
```
INFO:     Started server process [6592] 
INFO:     Waiting for application startup.
INFO:     Application startup complete.
INFO:     Uvicorn running on http://127.0.0.1:8000

```
- Check the swagger documentation at http://127.0.0.1:8000/docs for details about supported requests and valid format.

## Authors and acknowledgment
Deleped by Telerik Alpha Python 40 Team 6 - Valeria Nikolaeva, Pavel Petkov, Irena Russeva 

