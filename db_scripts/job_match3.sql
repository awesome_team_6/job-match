-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: job_match
-- ------------------------------------------------------
-- Server version	5.5.5-10.9.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cad_match_requests`
--

DROP TABLE IF EXISTS `cad_match_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cad_match_requests` (
  `Company_ads_id` int(11) NOT NULL,
  `Employers_id` int(11) NOT NULL,
  `is_matched` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Company_ads_id`,`Employers_id`),
  KEY `fk_Company_ads_has_Employers_Employers1_idx` (`Employers_id`),
  KEY `fk_Company_ads_has_Employers_Company_ads1_idx` (`Company_ads_id`),
  CONSTRAINT `fk_Company_ads_has_Employers_Company_ads1` FOREIGN KEY (`Company_ads_id`) REFERENCES `company_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Company_ads_has_Employers_Employers1` FOREIGN KEY (`Employers_id`) REFERENCES `employers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cad_match_requests`
--

LOCK TABLES `cad_match_requests` WRITE;
/*!40000 ALTER TABLE `cad_match_requests` DISABLE KEYS */;
INSERT INTO `cad_match_requests` VALUES (2,2,0),(2,3,0),(3,2,0),(3,3,0);
/*!40000 ALTER TABLE `cad_match_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_ads`
--

DROP TABLE IF EXISTS `company_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minimum_salary` int(11) NOT NULL,
  `maximum_salary` int(11) NOT NULL,
  `description` varchar(1001) CHARACTER SET utf8mb3 NOT NULL,
  `is_full_remote` tinyint(1) NOT NULL DEFAULT 0,
  `is_partial_remote` tinyint(1) NOT NULL DEFAULT 0,
  `is_main_ad` tinyint(1) NOT NULL DEFAULT 0,
  `Status_id` int(11) NOT NULL,
  `Professionals_id` int(11) NOT NULL,
  `Location_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Status_id`,`Professionals_id`,`Location_id`),
  KEY `fk_Company_ads_Status1_idx` (`Status_id`),
  KEY `fk_Company_ads_Professionals1_idx` (`Professionals_id`),
  KEY `fk_Company_ads_Location1_idx` (`Location_id`),
  CONSTRAINT `fk_Company_ads_Location1` FOREIGN KEY (`Location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Company_ads_Professionals1` FOREIGN KEY (`Professionals_id`) REFERENCES `professionals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Company_ads_Status1` FOREIGN KEY (`Status_id`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_ads`
--

LOCK TABLES `company_ads` WRITE;
/*!40000 ALTER TABLE `company_ads` DISABLE KEYS */;
INSERT INTO `company_ads` VALUES (2,1000,3000,'fresh graduate from TU-sofia with interests in Python',0,0,1,1,1,1),(3,1000,3000,'fresh graduate from TU-sofia with interests in Java',0,1,0,1,1,1);
/*!40000 ALTER TABLE `company_ads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(45) NOT NULL,
  `e_mail` varchar(101) NOT NULL,
  `twitter` varchar(101) DEFAULT NULL,
  `address` varchar(201) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'0888665221','hr@cola.bg','ttttd','kazan 16'),(2,'08886658803','hr@pepsi.bg','ttttdpepsi','Okolovrasten 102'),(3,'08886658403','hr@derbi.bg','ttttdderbi','Okolovrasten 2'),(4,'08886778403','hr@fanta.bg','twiterfanta','Rekata 3'),(5,'+359898123123','ira_russeva@abv.bg','test','123 Sofia str'),(6,'+359898123123','ira_russeva@abv.bg','test','123 Sofia str'),(7,'+359898123123','ira_russeva@abv.bg','test','123 Sofia str'),(8,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(9,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(10,'+359898123122','ira_russeva@abv.bg','test','123 Sofia str'),(11,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(12,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(13,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(14,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(15,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(16,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(17,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(18,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(19,'08886778403','hr@sprite.bg','twiterfanta','Rekata 4'),(20,'0886 66 21 55','hrsi@spt.com','twittertt','okolovrasnoto 75'),(21,'+359898177122','kireto@abv.bg','kireto_vtuitar','teva 8');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employers`
--

DROP TABLE IF EXISTS `employers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) CHARACTER SET utf8mb3 NOT NULL,
  `password` varchar(501) NOT NULL,
  `employer_name` varchar(40) CHARACTER SET utf8mb3 NOT NULL,
  `description` varchar(1001) CHARACTER SET utf8mb3 NOT NULL,
  `logo` varchar(501) CHARACTER SET utf8mb3 DEFAULT NULL,
  `Location_id` int(11) NOT NULL,
  `Contacts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Location_id`,`Contacts_id`),
  UNIQUE KEY `usernamne_UNIQUE` (`username`),
  UNIQUE KEY `employer_name_UNIQUE` (`employer_name`),
  KEY `fk_Employers_Location_idx` (`Location_id`),
  KEY `fk_Employers_Contacts1_idx` (`Contacts_id`),
  CONSTRAINT `fk_Employers_Contacts1` FOREIGN KEY (`Contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employers_Location` FOREIGN KEY (`Location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employers`
--

LOCK TABLES `employers` WRITE;
/*!40000 ALTER TABLE `employers` DISABLE KEYS */;
INSERT INTO `employers` VALUES (1,'Coke','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Coke Cola','Employer of the Year 2018','ssssjd',1,1),(2,'Pepsi','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Pepsico','Employer of the Year 2019','ssssjd',1,2),(3,'Derbi','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Derbi Cola','Employer of the Year 2020','ssssjd',2,3),(4,'Fanta','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Fanta Cola','cool company','ssssjd',2,4),(5,'Sprite','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Sprite Cola','coolest company','ssssjd.jpg',2,8),(6,'Sprite2','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Sprite Cola2','coolest company2','ssssjd.jpg',2,9),(11,'Sprite3','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Sprite Cola3','coolest company3','ssssjd.jpg',2,15),(15,'Sprite4','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Sprite Cola4','coolest company3','ssssjd.jpg',2,19),(16,'Sprite6','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Sprite6 cola','employer of the year 2010','somje new pic',1,20);
/*!40000 ALTER TABLE `employers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jad_match_requests`
--

DROP TABLE IF EXISTS `jad_match_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jad_match_requests` (
  `Professionals_id` int(11) NOT NULL,
  `Job_ads_id` int(11) NOT NULL,
  `is_matched` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Professionals_id`,`Job_ads_id`),
  KEY `fk_Professionals_has_Job_ads_Job_ads1_idx` (`Job_ads_id`),
  KEY `fk_Professionals_has_Job_ads_Professionals1_idx` (`Professionals_id`),
  CONSTRAINT `fk_Professionals_has_Job_ads_Job_ads1` FOREIGN KEY (`Job_ads_id`) REFERENCES `job_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Professionals_has_Job_ads_Professionals1` FOREIGN KEY (`Professionals_id`) REFERENCES `professionals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jad_match_requests`
--

LOCK TABLES `jad_match_requests` WRITE;
/*!40000 ALTER TABLE `jad_match_requests` DISABLE KEYS */;
INSERT INTO `jad_match_requests` VALUES (2,2,0);
/*!40000 ALTER TABLE `jad_match_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_ads`
--

DROP TABLE IF EXISTS `job_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minimum_salary` int(11) DEFAULT NULL,
  `maximum_salary` int(11) DEFAULT NULL,
  `description` varchar(1001) CHARACTER SET utf8mb3 DEFAULT NULL,
  `is_full_remote` tinyint(1) NOT NULL,
  `is_partial_remote` tinyint(1) NOT NULL,
  `Location_id` int(11) NOT NULL,
  `Status_id` int(11) NOT NULL,
  `Employers_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Location_id`,`Status_id`,`Employers_id`),
  KEY `fk_Ads_Location1_idx` (`Location_id`),
  KEY `fk_Ads_Status1_idx` (`Status_id`),
  KEY `fk_Ads_Employers1_idx` (`Employers_id`),
  CONSTRAINT `fk_Ads_Employers1` FOREIGN KEY (`Employers_id`) REFERENCES `employers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ads_Location1` FOREIGN KEY (`Location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ads_Status1` FOREIGN KEY (`Status_id`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_ads`
--

LOCK TABLES `job_ads` WRITE;
/*!40000 ALTER TABLE `job_ads` DISABLE KEYS */;
INSERT INTO `job_ads` VALUES (2,850,1250,'ohranitel na portala',0,0,1,1,1),(3,850,1250,'ohranitel na portala',0,0,1,1,1),(4,850,1250,'ohranitel na vhoda na administraciata',0,0,1,1,1),(5,1100,1200,'Obyava test 3',1,0,1,1,1),(6,5100,10200,'Junior scrum master',1,0,1,1,1),(7,2100,3200,'Junior Developer',0,1,1,1,1),(8,2500,3000,'Junior Python Developer',0,1,1,1,2),(9,2000,2600,'data analyst',0,0,2,1,3),(10,1500,3000,'Junior Developer',0,0,1,2,2);
/*!40000 ALTER TABLE `job_ads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `levels_of_qualification`
--

DROP TABLE IF EXISTS `levels_of_qualification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `levels_of_qualification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `levels_of_qualification`
--

LOCK TABLES `levels_of_qualification` WRITE;
/*!40000 ALTER TABLE `levels_of_qualification` DISABLE KEYS */;
INSERT INTO `levels_of_qualification` VALUES (1,'begginer'),(2,'intermediate'),(3,'expert');
/*!40000 ALTER TABLE `levels_of_qualification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) CHARACTER SET utf8mb3 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=282 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Sofia'),(2,'Plovdiv'),(3,'Bansko'),(4,'Belitsa'),(5,'Blagoevgrad'),(6,'Gotse Delchev'),(7,'Dobrinishte'),(8,'Kresna'),(9,'Melnik'),(10,'Petrich'),(11,'Razlog'),(12,'Sandanski'),(13,'Simitli'),(14,'Hadzhidimovo'),(15,'Yakoruda'),(16,'Aytos'),(17,'Ahtopol'),(18,'Bourgas'),(19,'Dolni Chiflik'),(20,'Kableshkovo'),(21,'Kameno'),(22,'Karnobat'),(23,'Kiten'),(24,'Malko Tarnovo'),(25,'Nessebar'),(26,'Obzor'),(27,'Pomorie'),(28,'Primorsko'),(29,'Saint Vlas'),(30,'Sozopol'),(31,'Sredets'),(32,'Sungulare'),(33,'Tsarevo'),(34,'Aheloy'),(35,'Bansko'),(36,'Belitsa'),(37,'Blagoevgrad'),(38,'Gotse Delchev'),(39,'Dobrinishte'),(40,'Kresna'),(41,'Melnik'),(42,'Petrich'),(43,'Razlog'),(44,'Sandanski'),(45,'Simitli'),(46,'Hadzhidimovo'),(47,'Yakoruda'),(48,'Aytos'),(49,'Ahtopol'),(50,'Bourgas'),(51,'Dolni Chiflik'),(52,'Kableshkovo'),(53,'Kameno'),(54,'Karnobat'),(55,'Kiten'),(56,'Malko Tarnovo'),(57,'Nessebar'),(58,'Obzor'),(59,'Pomorie'),(60,'Primorsko'),(61,'Saint Vlas'),(62,'Sozopol'),(63,'Sredets'),(64,'Sungulare'),(65,'Tsarevo'),(66,'Aheloy'),(67,'Aksakovo'),(68,'Beloslav'),(69,'Byala'),(70,'Varna'),(71,'Valchi Dol'),(72,'Devnya'),(73,'Dalgopol'),(74,'Provadiya'),(75,'Suvorovo'),(76,'Ignatievo'),(77,'Byala Cherkva'),(78,'Veliko Tarnovo'),(79,'Gorna Oryahovitsa'),(80,'Debelets'),(81,'Dolna Oryahovitsa'),(82,'Elena'),(83,'Zlataritsa'),(84,'Kilifarevo'),(85,'Lyaskovets'),(86,'Pavlikeni'),(87,'Polski Trambesh'),(88,'Svishtov'),(89,'Strazhitsa'),(90,'Suhindol'),(91,'Belogradchik'),(92,'Bregovo'),(93,'Vidin'),(94,'Gramada'),(95,'Dimovo'),(96,'Dunavtsi'),(97,'Kula'),(98,'Byala Slatina'),(99,'Vratsa'),(100,'Kozloduy'),(101,'Krivodol'),(102,'Mezdra'),(103,'Miziya'),(104,'Oryahovo'),(105,'Roman'),(106,'Gabrovo'),(107,'Dryanovo'),(108,'Plachkovtsi'),(109,'Sevlievo'),(110,'Tryavna'),(111,'Balchik'),(112,'General Toshevo'),(113,'Dobrich'),(114,'Kavarna'),(115,'Tervel'),(116,'Shabla'),(117,'Ardino'),(118,'Dzhebel'),(119,'Krumovgrad'),(120,'Kardzhali'),(121,'Momchilgrad'),(122,'Bobov Dol'),(123,'Boboshevo'),(124,'Dupnitsa'),(125,'Kocherinovo'),(126,'Kyustendil'),(127,'Rila'),(128,'Separeva Banya'),(129,'Apriltsi'),(130,'Letnitsa'),(131,'Lovech'),(132,'Lukovit'),(133,'Teteven'),(134,'Troyan'),(135,'Ugarchin'),(136,'Yablanitsa'),(137,'Berkovitsa'),(138,'Boychinovtsi'),(139,'Brusartsi'),(140,'Valchedram'),(141,'Varshets'),(142,'Lom'),(143,'Montana'),(144,'Chiprovtsi'),(145,'Batak'),(146,'Belovo'),(147,'Bratsigovo'),(148,'Velingrad'),(149,'Vetren'),(150,'Kostandovo'),(151,'Pazardzhik'),(152,'Panagyurishte'),(153,'Peshtera'),(154,'Rakitovo'),(155,'Septemvri'),(156,'Strelcha'),(157,'Sarnitsa'),(158,'Batanovtsi'),(159,'Breznik'),(160,'Zemen'),(161,'Pernik'),(162,'Radomir'),(163,'Tran'),(164,'Belene'),(165,'Gulyantsi'),(166,'Dolna Mitropoliya'),(167,'Dolni Dabnik'),(168,'Iskar'),(169,'Knezha'),(170,'Koynare'),(171,'Levski'),(172,'Nikopol'),(173,'Pleven'),(174,'Pordim'),(175,'Slavyanovo'),(176,'Trastenik'),(177,'Cherven bryag'),(178,'Asenovgrad'),(179,'Banya'),(180,'Brezovo'),(181,'Kalofer'),(182,'Karlovo'),(183,'Klisura'),(184,'Krichim'),(185,'Kuklen'),(186,'Laki'),(187,'Perushtitsa'),(189,'Parvomay'),(190,'Rakovski'),(191,'Sadovo'),(192,'Sopot'),(193,'Stamboliyski'),(194,'Saedinenie'),(195,'Hisarya'),(196,'Banya'),(197,'Zavet'),(198,'Isperih'),(199,'Kubrat'),(200,'Loznitsa'),(201,'Razgrad'),(202,'Tsar Kaloyan'),(203,'Borovo'),(204,'Byala'),(205,'Vetovo'),(206,'Glodzhevo'),(207,'Dve Mogili'),(208,'Marten'),(209,'Ruse'),(210,'Senovo'),(211,'Slivo pole'),(212,'Alfatar'),(213,'Glavinitsa'),(214,'Dulovo'),(215,'Silistra'),(216,'Tutrakan'),(217,'Kermen'),(218,'Kotel'),(219,'Nova Zagora'),(220,'Sliven'),(221,'Tvarditsa'),(222,'Shivachevo'),(223,'Devin'),(224,'Dospat'),(225,'Zlatograd'),(226,'Madan'),(227,'Nedelino'),(228,'Rudozem'),(229,'Smolyan'),(230,'Chepelare'),(231,'Bozhurishte'),(232,'Botevgrad'),(233,'Buhovo'),(234,'Balgarovo'),(235,'Godech'),(236,'Dolna Banya'),(237,'Dragoman'),(238,'Elin Pelin'),(239,'Etropole'),(240,'Zlatitsa'),(241,'Ihtiman'),(242,'Koprivshtitsa'),(243,'Kostenets'),(244,'Kostinbrod'),(245,'Momin prohod'),(246,'Novi Iskar'),(247,'Pirdop'),(248,'Pravets'),(249,'Samokov'),(250,'Svoge'),(251,'Slivnitsa'),(253,'Gurkovo'),(254,'Galabovo'),(255,'Kazanlak'),(256,'Maglizh'),(257,'Nikolaevo'),(258,'Pavel Banya'),(259,'Radnevo'),(260,'Stara Zagora'),(261,'Chirpan'),(262,'Shipka'),(263,'Antonovo'),(264,'Omurtag'),(265,'Opaka'),(266,'Popovo'),(267,'Targovishte'),(268,'Varbitsa'),(269,'Dimitrovgrad'),(270,'Ivaylovgrad'),(271,'Lyubimets'),(272,'Madzharovo'),(273,'Merichleri'),(274,'Svilengrad'),(275,'Simeonovgrad'),(276,'Topolovgrad'),(277,'Harmanli'),(278,'Haskovo'),(279,'Elhovo'),(280,'Straldzha'),(281,'Yambol');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_qualifications_levels`
--

DROP TABLE IF EXISTS `professional_qualifications_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professional_qualifications_levels` (
  `Qualifications_id` int(11) NOT NULL,
  `levels_of_qualification_id` int(11) NOT NULL,
  `Company_ads_id` int(11) NOT NULL,
  PRIMARY KEY (`Qualifications_id`,`levels_of_qualification_id`,`Company_ads_id`),
  KEY `fk_Qualifications_has_levels_of_qualification_levels_of_qua_idx` (`levels_of_qualification_id`),
  KEY `fk_Qualifications_has_levels_of_qualification_Qualification_idx` (`Qualifications_id`),
  KEY `fk_Qualifications_levels_Company_ads1_idx` (`Company_ads_id`),
  CONSTRAINT `fk_Qualifications_has_levels_of_qualification_Qualifications2` FOREIGN KEY (`Qualifications_id`) REFERENCES `qualifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Qualifications_has_levels_of_qualification_levels_of_quali2` FOREIGN KEY (`levels_of_qualification_id`) REFERENCES `levels_of_qualification` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Qualifications_levels_Company_ads1` FOREIGN KEY (`Company_ads_id`) REFERENCES `company_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_qualifications_levels`
--

LOCK TABLES `professional_qualifications_levels` WRITE;
/*!40000 ALTER TABLE `professional_qualifications_levels` DISABLE KEYS */;
INSERT INTO `professional_qualifications_levels` VALUES (1,2,2),(2,2,3);
/*!40000 ALTER TABLE `professional_qualifications_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professionals`
--

DROP TABLE IF EXISTS `professionals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professionals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb3 NOT NULL,
  `password` varchar(201) NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8mb3 NOT NULL,
  `last_name` varchar(50) CHARACTER SET utf8mb3 NOT NULL,
  `summary` varchar(1001) CHARACTER SET utf8mb3 NOT NULL,
  `is_busy` tinyint(1) NOT NULL DEFAULT 0,
  `foto` varchar(501) CHARACTER SET utf8mb3 DEFAULT NULL,
  `Location_id` int(11) NOT NULL,
  `Contacts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Location_id`,`Contacts_id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_Professionals_Location1_idx` (`Location_id`),
  KEY `fk_Professionals_Contacts1_idx` (`Contacts_id`),
  CONSTRAINT `fk_Professionals_Contacts1` FOREIGN KEY (`Contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Professionals_Location1` FOREIGN KEY (`Location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professionals`
--

LOCK TABLES `professionals` WRITE;
/*!40000 ALTER TABLE `professionals` DISABLE KEYS */;
INSERT INTO `professionals` VALUES (1,'Mira','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Mira','Russeva','Python Tigers Team',0,'test.png',2,7),(2,'Ira','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Iira','Russev','Python Tigers Team23',0,'nov test.png',2,10),(3,'Kiro','0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c','Kiro','Kirov','data analyst with 5 years experiance',0,'test2.png',161,21);
/*!40000 ALTER TABLE `professionals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualifications`
--

DROP TABLE IF EXISTS `qualifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualification` varchar(50) CHARACTER SET utf8mb3 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualifications`
--

LOCK TABLES `qualifications` WRITE;
/*!40000 ALTER TABLE `qualifications` DISABLE KEYS */;
INSERT INTO `qualifications` VALUES (1,'Python'),(2,'Java'),(3,'C sharp'),(4,'Security Guard'),(5,'Driver');
/*!40000 ALTER TABLE `qualifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualifications_levels`
--

DROP TABLE IF EXISTS `qualifications_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `qualifications_levels` (
  `Qualifications_id` int(11) NOT NULL,
  `levels_of_qualification_id` int(11) NOT NULL,
  `Job_ads_id` int(11) NOT NULL,
  PRIMARY KEY (`Qualifications_id`,`levels_of_qualification_id`,`Job_ads_id`),
  KEY `fk_Qualifications_has_levels_of_qualification_levels_of_qua_idx` (`levels_of_qualification_id`),
  KEY `fk_Qualifications_has_levels_of_qualification_Qualification_idx` (`Qualifications_id`),
  KEY `fk_Qualifications_levels_Job_ads1_idx` (`Job_ads_id`),
  CONSTRAINT `fk_Qualifications_has_levels_of_qualification_Qualifications1` FOREIGN KEY (`Qualifications_id`) REFERENCES `qualifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Qualifications_has_levels_of_qualification_levels_of_quali1` FOREIGN KEY (`levels_of_qualification_id`) REFERENCES `levels_of_qualification` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Qualifications_levels_Job_ads1` FOREIGN KEY (`Job_ads_id`) REFERENCES `job_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualifications_levels`
--

LOCK TABLES `qualifications_levels` WRITE;
/*!40000 ALTER TABLE `qualifications_levels` DISABLE KEYS */;
INSERT INTO `qualifications_levels` VALUES (1,1,7),(1,1,8),(1,1,10),(1,2,5),(1,3,6),(2,1,7),(2,1,8),(2,1,9),(2,1,10),(2,3,5),(2,3,6),(3,2,6),(4,1,3),(4,1,4);
/*!40000 ALTER TABLE `qualifications_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'active'),(2,'archived'),(3,'hidden'),(4,'private'),(5,'matched');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-04 19:56:49
