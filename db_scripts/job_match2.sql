-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: job_match
-- ------------------------------------------------------
-- Server version	5.5.5-10.9.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `company_ads`
--

DROP TABLE IF EXISTS `company_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minimum_salary` int(11) NOT NULL,
  `maximum_salary` int(11) NOT NULL,
  `description` varchar(1001) CHARACTER SET utf8mb3 NOT NULL,
  `is_full_remote` tinyint(1) NOT NULL DEFAULT 0,
  `is_partial_remote` tinyint(1) NOT NULL DEFAULT 0,
  `is_main_ad` tinyint(1) NOT NULL DEFAULT 0,
  `Status_id` int(11) NOT NULL,
  `Professionals_id` int(11) NOT NULL,
  `Location_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Status_id`,`Professionals_id`,`Location_id`),
  KEY `fk_Company_ads_Status1_idx` (`Status_id`),
  KEY `fk_Company_ads_Professionals1_idx` (`Professionals_id`),
  KEY `fk_Company_ads_Location1_idx` (`Location_id`),
  CONSTRAINT `fk_Company_ads_Location1` FOREIGN KEY (`Location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Company_ads_Professionals1` FOREIGN KEY (`Professionals_id`) REFERENCES `professionals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Company_ads_Status1` FOREIGN KEY (`Status_id`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_ads`
--

LOCK TABLES `company_ads` WRITE;
/*!40000 ALTER TABLE `company_ads` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_ads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_ads_matches`
--

DROP TABLE IF EXISTS `company_ads_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company_ads_matches` (
  `Company_ads_id` int(11) NOT NULL,
  `Employers_id` int(11) NOT NULL,
  PRIMARY KEY (`Company_ads_id`,`Employers_id`),
  KEY `fk_Company_ads_has_Employers_Employers1_idx` (`Employers_id`),
  KEY `fk_Company_ads_has_Employers_Company_ads1_idx` (`Company_ads_id`),
  CONSTRAINT `fk_Company_ads_has_Employers_Company_ads1` FOREIGN KEY (`Company_ads_id`) REFERENCES `company_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Company_ads_has_Employers_Employers1` FOREIGN KEY (`Employers_id`) REFERENCES `employers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_ads_matches`
--

LOCK TABLES `company_ads_matches` WRITE;
/*!40000 ALTER TABLE `company_ads_matches` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_ads_matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(45) NOT NULL,
  `e_mail` varchar(101) NOT NULL,
  `twitter` varchar(101) DEFAULT NULL,
  `address` varchar(201) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (13,'0888665511','hr@coke.bg','tttt','ul raico kazandjiata 8');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employers`
--

DROP TABLE IF EXISTS `employers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) CHARACTER SET utf8mb3 NOT NULL,
  `password` varchar(501) NOT NULL,
  `employer_name` varchar(40) CHARACTER SET utf8mb3 NOT NULL,
  `description` varchar(1001) CHARACTER SET utf8mb3 NOT NULL,
  `logo` varchar(501) CHARACTER SET utf8mb3 DEFAULT NULL,
  `Location_id` int(11) NOT NULL,
  `Contacts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Location_id`,`Contacts_id`),
  UNIQUE KEY `usernamne_UNIQUE` (`username`),
  UNIQUE KEY `employer_name_UNIQUE` (`employer_name`),
  KEY `fk_Employers_Location_idx` (`Location_id`),
  KEY `fk_Employers_Contacts1_idx` (`Contacts_id`),
  CONSTRAINT `fk_Employers_Contacts1` FOREIGN KEY (`Contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Employers_Location` FOREIGN KEY (`Location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employers`
--

LOCK TABLES `employers` WRITE;
/*!40000 ALTER TABLE `employers` DISABLE KEYS */;
INSERT INTO `employers` VALUES (1,'coke','d17f25ecfbcc7857f7bebea469308be0b2580943e96d13a3ad98a13675c4bfc2','Coke Cola','Employer of the Year 2005','ssssj',1,13);
/*!40000 ALTER TABLE `employers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_ads`
--

DROP TABLE IF EXISTS `job_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minimum_salary` int(11) DEFAULT NULL,
  `maximum_salary` int(11) DEFAULT NULL,
  `description` varchar(1001) CHARACTER SET utf8mb3 DEFAULT NULL,
  `is_full_remote` tinyint(1) NOT NULL,
  `is_partial_remote` tinyint(1) NOT NULL,
  `Location_id` int(11) NOT NULL,
  `Status_id` int(11) NOT NULL,
  `Employers_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Location_id`,`Status_id`,`Employers_id`),
  KEY `fk_Ads_Location1_idx` (`Location_id`),
  KEY `fk_Ads_Status1_idx` (`Status_id`),
  KEY `fk_Ads_Employers1_idx` (`Employers_id`),
  CONSTRAINT `fk_Ads_Employers1` FOREIGN KEY (`Employers_id`) REFERENCES `employers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ads_Location1` FOREIGN KEY (`Location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ads_Status1` FOREIGN KEY (`Status_id`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_ads`
--

LOCK TABLES `job_ads` WRITE;
/*!40000 ALTER TABLE `job_ads` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_ads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_ads_matches`
--

DROP TABLE IF EXISTS `job_ads_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `job_ads_matches` (
  `Professionals_id` int(11) NOT NULL,
  `Job_ads_id` int(11) NOT NULL,
  PRIMARY KEY (`Professionals_id`,`Job_ads_id`),
  KEY `fk_Professionals_has_Job_ads_Job_ads1_idx` (`Job_ads_id`),
  KEY `fk_Professionals_has_Job_ads_Professionals1_idx` (`Professionals_id`),
  CONSTRAINT `fk_Professionals_has_Job_ads_Job_ads1` FOREIGN KEY (`Job_ads_id`) REFERENCES `job_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Professionals_has_Job_ads_Professionals1` FOREIGN KEY (`Professionals_id`) REFERENCES `professionals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_ads_matches`
--

LOCK TABLES `job_ads_matches` WRITE;
/*!40000 ALTER TABLE `job_ads_matches` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_ads_matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `levels_of_qualification`
--

DROP TABLE IF EXISTS `levels_of_qualification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `levels_of_qualification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `levels_of_qualification`
--

LOCK TABLES `levels_of_qualification` WRITE;
/*!40000 ALTER TABLE `levels_of_qualification` DISABLE KEYS */;
/*!40000 ALTER TABLE `levels_of_qualification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) CHARACTER SET utf8mb3 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Sofia'),(2,'Plovdiv');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_qualifications_levels`
--

DROP TABLE IF EXISTS `professional_qualifications_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professional_qualifications_levels` (
  `Qualifications_id` int(11) NOT NULL,
  `levels_of_qualification_id` int(11) NOT NULL,
  `Company_ads_id` int(11) NOT NULL,
  PRIMARY KEY (`Qualifications_id`,`levels_of_qualification_id`,`Company_ads_id`),
  KEY `fk_Qualifications_has_levels_of_qualification_levels_of_qua_idx` (`levels_of_qualification_id`),
  KEY `fk_Qualifications_has_levels_of_qualification_Qualification_idx` (`Qualifications_id`),
  KEY `fk_Qualifications_levels_Company_ads1_idx` (`Company_ads_id`),
  CONSTRAINT `fk_Qualifications_has_levels_of_qualification_Qualifications2` FOREIGN KEY (`Qualifications_id`) REFERENCES `qualifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Qualifications_has_levels_of_qualification_levels_of_quali2` FOREIGN KEY (`levels_of_qualification_id`) REFERENCES `levels_of_qualification` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Qualifications_levels_Company_ads1` FOREIGN KEY (`Company_ads_id`) REFERENCES `company_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_qualifications_levels`
--

LOCK TABLES `professional_qualifications_levels` WRITE;
/*!40000 ALTER TABLE `professional_qualifications_levels` DISABLE KEYS */;
/*!40000 ALTER TABLE `professional_qualifications_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professionals`
--

DROP TABLE IF EXISTS `professionals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `professionals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb3 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8mb3 NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8mb3 NOT NULL,
  `last_name` varchar(50) CHARACTER SET utf8mb3 NOT NULL,
  `summary` varchar(1001) CHARACTER SET utf8mb3 NOT NULL,
  `is_busy` tinyint(1) NOT NULL DEFAULT 0,
  `foto` varchar(501) CHARACTER SET utf8mb3 DEFAULT NULL,
  `Location_id` int(11) NOT NULL,
  `Contacts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`Location_id`,`Contacts_id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_Professionals_Location1_idx` (`Location_id`),
  KEY `fk_Professionals_Contacts1_idx` (`Contacts_id`),
  CONSTRAINT `fk_Professionals_Contacts1` FOREIGN KEY (`Contacts_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Professionals_Location1` FOREIGN KEY (`Location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professionals`
--

LOCK TABLES `professionals` WRITE;
/*!40000 ALTER TABLE `professionals` DISABLE KEYS */;
/*!40000 ALTER TABLE `professionals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualifications`
--

DROP TABLE IF EXISTS `qualifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qualification` varchar(50) CHARACTER SET utf8mb3 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualifications`
--

LOCK TABLES `qualifications` WRITE;
/*!40000 ALTER TABLE `qualifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `qualifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualifications_levels`
--

DROP TABLE IF EXISTS `qualifications_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `qualifications_levels` (
  `Qualifications_id` int(11) NOT NULL,
  `levels_of_qualification_id` int(11) NOT NULL,
  `Job_ads_id` int(11) NOT NULL,
  PRIMARY KEY (`Qualifications_id`,`levels_of_qualification_id`,`Job_ads_id`),
  KEY `fk_Qualifications_has_levels_of_qualification_levels_of_qua_idx` (`levels_of_qualification_id`),
  KEY `fk_Qualifications_has_levels_of_qualification_Qualification_idx` (`Qualifications_id`),
  KEY `fk_Qualifications_levels_Job_ads1_idx` (`Job_ads_id`),
  CONSTRAINT `fk_Qualifications_has_levels_of_qualification_Qualifications1` FOREIGN KEY (`Qualifications_id`) REFERENCES `qualifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Qualifications_has_levels_of_qualification_levels_of_quali1` FOREIGN KEY (`levels_of_qualification_id`) REFERENCES `levels_of_qualification` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Qualifications_levels_Job_ads1` FOREIGN KEY (`Job_ads_id`) REFERENCES `job_ads` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualifications_levels`
--

LOCK TABLES `qualifications_levels` WRITE;
/*!40000 ALTER TABLE `qualifications_levels` DISABLE KEYS */;
/*!40000 ALTER TABLE `qualifications_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-23  2:39:56
