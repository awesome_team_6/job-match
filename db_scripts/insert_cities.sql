DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `locations` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `city` varchar(50) CHARACTER SET utf8mb3 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO locations(city) VALUES('Sofia');
INSERT INTO locations(city) VALUES('Plovdiv');


INSERT INTO locations(city) VALUES('Bansko');
INSERT INTO locations(city) VALUES("Belitsa");
INSERT INTO locations(city) VALUES("Blagoevgrad");
INSERT INTO locations(city) VALUES("Gotse Delchev");
INSERT INTO locations(city) VALUES("Dobrinishte");
INSERT INTO locations(city) VALUES("Kresna");
INSERT INTO locations(city) VALUES("Melnik");
INSERT INTO locations(city) VALUES("Petrich");
INSERT INTO locations(city) VALUES("Razlog");
INSERT INTO locations(city) VALUES("Sandanski");
INSERT INTO locations(city) VALUES("Simitli");
INSERT INTO locations(city) VALUES("Hadzhidimovo");
INSERT INTO locations(city) VALUES("Yakoruda");

INSERT INTO locations(city) VALUES("Aytos");
INSERT INTO locations(city) VALUES("Ahtopol");
INSERT INTO locations(city) VALUES("Bourgas");
INSERT INTO locations(city) VALUES("Dolni Chiflik");
INSERT INTO locations(city) VALUES("Kableshkovo");
INSERT INTO locations(city) VALUES("Kameno");
INSERT INTO locations(city) VALUES("Karnobat");
INSERT INTO locations(city) VALUES("Kiten");
INSERT INTO locations(city) VALUES('Malko Tarnovo');
INSERT INTO locations(city) VALUES('Nessebar');
INSERT INTO locations(city) VALUES('Obzor');
INSERT INTO locations(city) VALUES('Pomorie');
INSERT INTO locations(city) VALUES('Primorsko');
INSERT INTO locations(city) VALUES('Saint Vlas');
INSERT INTO locations(city) VALUES('Sozopol');
INSERT INTO locations(city) VALUES('Sredets');
INSERT INTO locations(city) VALUES('Sungulare');
INSERT INTO locations(city) VALUES('Tsarevo');
INSERT INTO locations(city) VALUES('Aheloy');


INSERT INTO locations(city) VALUES('Aksakovo');
INSERT INTO locations(city) VALUES('Beloslav');
INSERT INTO locations(city) VALUES('Byala');
INSERT INTO locations(city) VALUES('Varna');
INSERT INTO locations(city) VALUES('Valchi Dol');
INSERT INTO locations(city) VALUES('Devnya');
INSERT INTO locations(city) VALUES('Dalgopol');
INSERT INTO locations(city) VALUES('Provadiya');
INSERT INTO locations(city) VALUES('Suvorovo');
INSERT INTO locations(city) VALUES('Ignatievo');

INSERT INTO locations(city) VALUES('Byala Cherkva');
INSERT INTO locations(city) VALUES('Veliko Tarnovo');
INSERT INTO locations(city) VALUES('Gorna Oryahovitsa');
INSERT INTO locations(city) VALUES('Debelets');
INSERT INTO locations(city) VALUES('Dolna Oryahovitsa');
INSERT INTO locations(city) VALUES('Elena');
INSERT INTO locations(city) VALUES('Zlataritsa');
INSERT INTO locations(city) VALUES('Kilifarevo');
INSERT INTO locations(city) VALUES('Lyaskovets');
INSERT INTO locations(city) VALUES('Pavlikeni');
INSERT INTO locations(city) VALUES('Polski Trambesh');
INSERT INTO locations(city) VALUES('Svishtov');
INSERT INTO locations(city) VALUES('Strazhitsa');
INSERT INTO locations(city) VALUES('Suhindol');

INSERT INTO locations(city) VALUES('Belogradchik');
INSERT INTO locations(city) VALUES('Bregovo');
INSERT INTO locations(city) VALUES('Vidin');
INSERT INTO locations(city) VALUES('Gramada');
INSERT INTO locations(city) VALUES('Dimovo');
INSERT INTO locations(city) VALUES('Dunavtsi');
INSERT INTO locations(city) VALUES('Kula');

INSERT INTO locations(city) VALUES('Byala Slatina');
INSERT INTO locations(city) VALUES('Vratsa');
INSERT INTO locations(city) VALUES('Kozloduy');
INSERT INTO locations(city) VALUES('Krivodol');
INSERT INTO locations(city) VALUES('Mezdra');
INSERT INTO locations(city) VALUES('Miziya');
INSERT INTO locations(city) VALUES('Oryahovo');
INSERT INTO locations(city) VALUES('Roman');


INSERT INTO locations(city) VALUES('Gabrovo');
INSERT INTO locations(city) VALUES('Dryanovo');
INSERT INTO locations(city) VALUES('Plachkovtsi');
INSERT INTO locations(city) VALUES('Sevlievo');
INSERT INTO locations(city) VALUES('Tryavna');


INSERT INTO locations(city) VALUES('Balchik');
INSERT INTO locations(city) VALUES('General Toshevo');
INSERT INTO locations(city) VALUES('Dobrich');
INSERT INTO locations(city) VALUES('Kavarna');
INSERT INTO locations(city) VALUES('Tervel');
INSERT INTO locations(city) VALUES('Shabla');


INSERT INTO locations(city) VALUES('Ardino');
INSERT INTO locations(city) VALUES('Dzhebel');
INSERT INTO locations(city) VALUES('Krumovgrad');
INSERT INTO locations(city) VALUES('Kardzhali');
INSERT INTO locations(city) VALUES('Momchilgrad');


INSERT INTO locations(city) VALUES('Bobov Dol');
INSERT INTO locations(city) VALUES('Boboshevo');
INSERT INTO locations(city) VALUES('Dupnitsa');
INSERT INTO locations(city) VALUES('Kocherinovo');
INSERT INTO locations(city) VALUES('Kyustendil');
INSERT INTO locations(city) VALUES('Rila');
INSERT INTO locations(city) VALUES('Separeva Banya');


INSERT INTO locations(city) VALUES('Apriltsi');
INSERT INTO locations(city) VALUES('Letnitsa');
INSERT INTO locations(city) VALUES('Lovech');
INSERT INTO locations(city) VALUES('Lukovit');
INSERT INTO locations(city) VALUES('Teteven');
INSERT INTO locations(city) VALUES('Troyan');
INSERT INTO locations(city) VALUES('Ugarchin');
INSERT INTO locations(city) VALUES('Yablanitsa');

INSERT INTO locations(city) VALUES('Berkovitsa');
INSERT INTO locations(city) VALUES('Boychinovtsi');
INSERT INTO locations(city) VALUES('Brusartsi');
INSERT INTO locations(city) VALUES('Valchedram');
INSERT INTO locations(city) VALUES('Varshets');
INSERT INTO locations(city) VALUES('Lom');
INSERT INTO locations(city) VALUES('Montana');
INSERT INTO locations(city) VALUES('Chiprovtsi');


INSERT INTO locations(city) VALUES('Batak');
INSERT INTO locations(city) VALUES('Belovo');
INSERT INTO locations(city) VALUES('Bratsigovo');
INSERT INTO locations(city) VALUES('Velingrad');
INSERT INTO locations(city) VALUES('Vetren');
INSERT INTO locations(city) VALUES('Kostandovo');
INSERT INTO locations(city) VALUES('Pazardzhik');
INSERT INTO locations(city) VALUES('Panagyurishte');
INSERT INTO locations(city) VALUES('Peshtera');
INSERT INTO locations(city) VALUES('Rakitovo');
INSERT INTO locations(city) VALUES('Septemvri');
INSERT INTO locations(city) VALUES('Strelcha');
INSERT INTO locations(city) VALUES('Sarnitsa');


INSERT INTO locations(city) VALUES('Batanovtsi');
INSERT INTO locations(city) VALUES('Breznik');
INSERT INTO locations(city) VALUES('Zemen');
INSERT INTO locations(city) VALUES('Pernik');
INSERT INTO locations(city) VALUES('Radomir');
INSERT INTO locations(city) VALUES('Tran');


INSERT INTO locations(city) VALUES('Belene');
INSERT INTO locations(city) VALUES('Gulyantsi');
INSERT INTO locations(city) VALUES('Dolna Mitropoliya');
INSERT INTO locations(city) VALUES('Dolni Dabnik');
INSERT INTO locations(city) VALUES('Iskar');
INSERT INTO locations(city) VALUES('Knezha');
INSERT INTO locations(city) VALUES('Koynare');
INSERT INTO locations(city) VALUES('Levski');
INSERT INTO locations(city) VALUES('Nikopol');
INSERT INTO locations(city) VALUES('Pleven');
INSERT INTO locations(city) VALUES('Pordim');
INSERT INTO locations(city) VALUES('Slavyanovo');
INSERT INTO locations(city) VALUES('Trastenik');
INSERT INTO locations(city) VALUES('Cherven bryag');


INSERT INTO locations(city) VALUES('Asenovgrad');
INSERT INTO locations(city) VALUES('Banya');
INSERT INTO locations(city) VALUES('Brezovo');
INSERT INTO locations(city) VALUES('Kalofer');
INSERT INTO locations(city) VALUES('Karlovo');
INSERT INTO locations(city) VALUES('Klisura');
INSERT INTO locations(city) VALUES('Krichim');
INSERT INTO locations(city) VALUES('Kuklen');
INSERT INTO locations(city) VALUES('Laki');
INSERT INTO locations(city) VALUES('Perushtitsa');
INSERT INTO locations(city) VALUES('Parvomay');
INSERT INTO locations(city) VALUES('Rakovski');
INSERT INTO locations(city) VALUES('Sadovo');
INSERT INTO locations(city) VALUES('Sopot');
INSERT INTO locations(city) VALUES('Stamboliyski');
INSERT INTO locations(city) VALUES('Saedinenie');
INSERT INTO locations(city) VALUES('Hisarya');
INSERT INTO locations(city) VALUES('Banya');


INSERT INTO locations(city) VALUES('Zavet');
INSERT INTO locations(city) VALUES('Isperih');
INSERT INTO locations(city) VALUES('Kubrat');
INSERT INTO locations(city) VALUES('Loznitsa');
INSERT INTO locations(city) VALUES('Razgrad');
INSERT INTO locations(city) VALUES('Tsar Kaloyan');


INSERT INTO locations(city) VALUES('Borovo');
INSERT INTO locations(city) VALUES('Byala');
INSERT INTO locations(city) VALUES('Vetovo');
INSERT INTO locations(city) VALUES('Glodzhevo');
INSERT INTO locations(city) VALUES('Dve Mogili');
INSERT INTO locations(city) VALUES('Marten');
INSERT INTO locations(city) VALUES('Ruse');
INSERT INTO locations(city) VALUES('Senovo');
INSERT INTO locations(city) VALUES('Slivo pole');


INSERT INTO locations(city) VALUES('Alfatar');
INSERT INTO locations(city) VALUES('Glavinitsa');
INSERT INTO locations(city) VALUES('Dulovo');
INSERT INTO locations(city) VALUES('Silistra');
INSERT INTO locations(city) VALUES('Tutrakan');


INSERT INTO locations(city) VALUES('Kermen');
INSERT INTO locations(city) VALUES('Kotel');
INSERT INTO locations(city) VALUES('Nova Zagora');
INSERT INTO locations(city) VALUES('Sliven');
INSERT INTO locations(city) VALUES('Tvarditsa');
INSERT INTO locations(city) VALUES('Shivachevo');

INSERT INTO locations(city) VALUES('Devin');
INSERT INTO locations(city) VALUES('Dospat');
INSERT INTO locations(city) VALUES('Zlatograd');
INSERT INTO locations(city) VALUES('Madan');
INSERT INTO locations(city) VALUES('Nedelino');
INSERT INTO locations(city) VALUES('Rudozem');
INSERT INTO locations(city) VALUES('Smolyan');
INSERT INTO locations(city) VALUES('Chepelare');


INSERT INTO cities(city) VALUES('Bankya');
INSERT INTO locations(city) VALUES('Bozhurishte');
INSERT INTO locations(city) VALUES('Botevgrad');
INSERT INTO locations(city) VALUES('Buhovo');
INSERT INTO locations(city) VALUES('Balgarovo');
INSERT INTO locations(city) VALUES('Godech');
INSERT INTO locations(city) VALUES('Dolna Banya');
INSERT INTO locations(city) VALUES('Dragoman');
INSERT INTO locations(city) VALUES('Elin Pelin');
INSERT INTO locations(city) VALUES('Etropole');
INSERT INTO locations(city) VALUES('Zlatitsa');
INSERT INTO locations(city) VALUES('Ihtiman');
INSERT INTO locations(city) VALUES('Koprivshtitsa');
INSERT INTO locations(city) VALUES('Kostenets');
INSERT INTO locations(city) VALUES('Kostinbrod');
INSERT INTO locations(city) VALUES('Momin prohod');
INSERT INTO locations(city) VALUES('Novi Iskar');
INSERT INTO locations(city) VALUES('Pirdop');
INSERT INTO locations(city) VALUES('Pravets');
INSERT INTO locations(city) VALUES('Samokov');
INSERT INTO locations(city) VALUES('Svoge');
INSERT INTO locations(city) VALUES('Slivnitsa');

INSERT INTO locations(city) VALUES('Gurkovo');
INSERT INTO locations(city) VALUES('Galabovo');
INSERT INTO locations(city) VALUES('Kazanlak');
INSERT INTO locations(city) VALUES('Maglizh');
INSERT INTO locations(city) VALUES('Nikolaevo');
INSERT INTO locations(city) VALUES('Pavel Banya');
INSERT INTO locations(city) VALUES('Radnevo');
INSERT INTO locations(city) VALUES('Stara Zagora');
INSERT INTO locations(city) VALUES('Chirpan');
INSERT INTO locations(city) VALUES('Shipka');

INSERT INTO locations(city) VALUES('Antonovo');
INSERT INTO locations(city) VALUES('Omurtag');
INSERT INTO locations(city) VALUES('Opaka');
INSERT INTO locations(city) VALUES('Popovo');
INSERT INTO locations(city) VALUES('Targovishte');

INSERT INTO locations(city) VALUES('Varbitsa');
INSERT INTO locations(city) VALUES('Dimitrovgrad');
INSERT INTO locations(city) VALUES('Ivaylovgrad');
INSERT INTO locations(city) VALUES('Lyubimets');
INSERT INTO locations(city) VALUES('Madzharovo');
INSERT INTO locations(city) VALUES('Merichleri');
INSERT INTO locations(city) VALUES('Svilengrad');
INSERT INTO locations(city) VALUES('Simeonovgrad');
INSERT INTO locations(city) VALUES('Topolovgrad');
INSERT INTO locations(city) VALUES('Harmanli');
INSERT INTO locations(city) VALUES('Haskovo');

INSERT INTO locations(city) VALUES('Elhovo');
INSERT INTO locations(city) VALUES('Straldzha');
INSERT INTO locations(city) VALUES('Yambol');
